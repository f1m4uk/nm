export const environment = {
  production: true,
  API_URL: 'https://desolate-everglades-80255.herokuapp.com/api',
  ADMIN_PHONE_NUMBER: "+3809812312345"
};
