import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Service } from './service.model';
import { Observable } from 'rxjs';

const SERVICES_URL = environment.API_URL + '/services';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  constructor(private httpClient: HttpClient) { }

  createService(body): Observable<{message: string, result: Service}> {
    const formData = new FormData();
    for (let field in body) {
      const value = body[field];
      if (Array.isArray(value)) {
        for (let valueField of value) {
          if (valueField !== null) {
            formData.append(field, valueField);
          }
        }
      } else {
        if (value !== null) {
          formData.append(field, value);
        }
      }
    }
    return this.httpClient.post<{message: string, result: Service}>(SERVICES_URL, formData);
  }

  deleteService(id: string): Observable<Service> {
    return this.httpClient.delete<Service>(SERVICES_URL + '/' + id);
  }

  getAdditionalServices(): Observable<Service[]> {
    const options = {
      params: new HttpParams().set('isAdditionalService', 'true')
    }

    return this.httpClient.get<Service[]>(SERVICES_URL, options);
  }

  getHotelServices(): Observable<Service[]> {
    const options = {
      params: new HttpParams().set('isHotelService', 'true')
    }

    return this.httpClient.get<Service[]>(SERVICES_URL, options);
  }

  getRoomsServices(): Observable<Service[]> {
    const options = {
      params: new HttpParams().set('isRoomService', 'true')
    }

    return this.httpClient.get<Service[]>(SERVICES_URL, options);
  }

  getServices(): Observable<Service[]> {
    return this.httpClient.get<Service[]>(SERVICES_URL);
  }

  getService(id: string): Observable<Service> {
    return this.httpClient.get<Service>(SERVICES_URL + '/' + id);
  }

  updateService(id: string, body): Observable<{message: string, result: Service}> {
    const formData = new FormData();
    for (let field in body) {
      const value = body[field];
      if (Array.isArray(value) && value.length > 0) {
        for (let valueField of value) {
          formData.append(field, valueField);
        }
      } else {
        formData.append(field, value);
      }
    }
    return this.httpClient.put<{message: string, result: Service}>(SERVICES_URL + '/' + id, formData);
  }
}
