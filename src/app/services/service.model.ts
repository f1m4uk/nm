export interface Service {
  _id: string,
  icon?: string,
  name_en?: string,
  name_ru: string,
  name_uk?: string,
  fullDescription_en?: string,
  fullDescription_ru?: string,
  fullDescription_uk?: string,
  shortDescription_en?: string,
  shortDescription_ru?: string,
  shortDescription_uk?: string,
  isRoomService?: boolean,
  isHotelService?: boolean,
  isAdditionalService?: boolean,
  imagePaths?: string[],
  images?: any[]
}
