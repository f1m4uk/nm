import { Component, OnInit, OnDestroy } from '@angular/core';
import { HandsetRuleDialogComponent } from './handset-rule-dialog/handset-rule-dialog.component';
import { Rule } from 'src/app/rules/rule.model';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Observable, BehaviorSubject, Subject, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, switchMapTo } from 'rxjs/operators';
import { RulesService } from 'src/app/rules/rules.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})
export class RulesComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetRuleDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  rulesSubject = new BehaviorSubject<Rule[]>([]);
  rules$: Observable<Rule[]> = this.rulesSubject.pipe(
    switchMapTo(this.rulesService.getRules())
  );
  selectedRuleSubject = new Subject<Rule>();
  selectedRule$: Observable<Rule> = this.selectedRuleSubject.asObservable();

  selectedRuleOnDevice: Subscription;

  window: Window = window;

  constructor(
    private rulesService: RulesService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedRuleOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedRuleOnDevice = combineLatest(this.isHandset$, this.selectedRule$).subscribe(info => {
      const isHandset = info[0];
      const selectedRule = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetRuleDialogComponent, {
          id: 'handset-rule-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            rule: selectedRule
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.rulesSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }
}
