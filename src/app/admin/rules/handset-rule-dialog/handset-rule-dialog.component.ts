import { Component, OnInit, Inject } from '@angular/core';
import { Rule } from 'src/app/rules/rule.model';
import { Observable } from 'rxjs';
import { RulesService } from 'src/app/rules/rules.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-rule-dialog',
  templateUrl: './handset-rule-dialog.component.html',
  styleUrls: ['./handset-rule-dialog.component.css']
})
export class HandsetRuleDialogComponent implements OnInit {
  rule$: Observable<Rule> = this.data.rule ? this.rulesService.getRule(this.data.rule._id) : null;

  constructor(
    private rulesService: RulesService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetRuleDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }
}
