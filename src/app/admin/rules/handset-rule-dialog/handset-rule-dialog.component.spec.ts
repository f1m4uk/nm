import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetRuleDialogComponent } from './handset-rule-dialog.component';

describe('HandsetRuleDialogComponent', () => {
  let component: HandsetRuleDialogComponent;
  let fixture: ComponentFixture<HandsetRuleDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetRuleDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetRuleDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
