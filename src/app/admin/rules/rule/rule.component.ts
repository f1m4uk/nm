import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Rule } from 'src/app/rules/rule.model';
import { RulesService } from 'src/app/rules/rules.service';
import { TranslocoService } from '@ngneat/transloco';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetRuleDialogComponent } from '../handset-rule-dialog/handset-rule-dialog.component';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.css']
})
export class RuleComponent implements OnInit, OnChanges, OnDestroy {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  form = new FormGroup({
    description_en: new FormControl(),
    description_ru: new FormControl(null, {validators: [Validators.required]}),
    description_uk: new FormControl()
  });
  @Input() ruleId: string;

  rule$: Observable<Rule>;

  @Output() ruleAdded = new EventEmitter<Rule>();
  ruleByLanguageSubscription: Subscription;
  @Output() ruleDeleted = new EventEmitter<string>();

  constructor(
    private rulesService: RulesService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService,
    public handsetRuleDialogRef: MatDialogRef<HandsetRuleDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.ruleId) {
      if (changes.ruleId.currentValue !== null) {
        this.rule$ = this.rulesService.getRule(changes.ruleId.currentValue).pipe(tap(rule => {
          this.form.patchValue(rule);
        }), shareReplay());
        this.ruleByLanguageSubscription = combineLatest(this.rule$, this.language$).subscribe(result => {
          const rule = result[0];
          if (this.rule$ !== null) {
            this.form.patchValue(rule);
          }
        });
      } else {
        this.rule$ = null;
        this.form.reset();
      }
    }
  }

  ngOnDestroy(): void {
    this.ruleByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onRuleDelete() {
    if (this.rule$) {
      this.isLoading = true;
      this.rulesService.deleteRule(this.ruleId).subscribe(result => {
        this.isLoading = false;
        this.ruleDeleted.emit(this.ruleId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onSubmitRule() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newRule$: Observable<{message: string, result: Rule}>;
    if (this.rule$) {
      newRule$ = this.rulesService.updateRule(this.ruleId, this.form.value);
    } else {
      newRule$ = this.rulesService.createRule(this.form.value);
    }
    newRule$.subscribe(response => {
      this.isLoading = false;
      this.ruleAdded.emit(response.result);
      if (this.handsetRuleDialogRef?.close) {
        this.handsetRuleDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }
}
