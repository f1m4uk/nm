import { Component, OnInit, OnChanges, OnDestroy, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Room } from 'src/app/rooms/room.model';
import { RoomsService } from 'src/app/rooms/rooms.service';
import { TranslocoService } from '@ngneat/transloco';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetRoomDialogComponent } from '../handset-room-dialog/handset-room-dialog.component';
import { RoomType } from 'src/app/room-types/room-type.model';
import { RoomTypesService } from 'src/app/room-types/room-types.service';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.css']
})
export class RoomComponent implements OnInit, OnChanges, OnDestroy {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  form = new FormGroup({
    name_en: new FormControl(),
    name_ru: new FormControl(null, {validators: [Validators.required]}),
    name_uk: new FormControl(),
    typeId: new FormControl()
  });
  @Input() roomId: string;

  room$: Observable<Room>;

  @Output() roomAdded = new EventEmitter<Room>();
  roomByLanguageSubscription: Subscription;
  @Output() roomDeleted = new EventEmitter<string>();
  roomTypes$: Observable<RoomType[]> = this.roomTypesService.getRoomTypes().pipe(shareReplay());

  constructor(
    private roomsService: RoomsService,
    private roomTypesService: RoomTypesService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService,
    public handsetRoomDialogRef: MatDialogRef<HandsetRoomDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.roomId) {
      if (changes.roomId.currentValue !== null) {
        this.room$ = this.roomsService.getRoom(changes.roomId.currentValue).pipe(tap(room => {
          this.form.patchValue(room);
          this.form.patchValue({
            typeId: room.type._id
          });
        }), shareReplay());
        this.roomByLanguageSubscription = combineLatest(this.room$, this.language$).subscribe(result => {
          const room = result[0];
          if (this.room$ !== null) {
            this.form.patchValue(room);
            this.form.patchValue({
              typeId: room.type._id
            });
          }
        });
      } else {
        this.room$ = null;
        this.form.reset();
      }
    }
  }

  ngOnDestroy(): void {
    this.roomByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onRoomDelete() {
    if (this.room$) {
      this.isLoading = true;
      this.roomsService.deleteRoom(this.roomId).subscribe(result => {
        this.isLoading = false;
        this.roomDeleted.emit(this.roomId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onSubmitRoom() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newRoom$: Observable<{message: string, result: Room}>;
    if (this.room$) {
      newRoom$ = this.roomsService.updateRoom(this.roomId, this.form.value);
    } else {
      newRoom$ = this.roomsService.createRoom(this.form.value);
    }
    newRoom$.subscribe(response => {
      this.isLoading = false;
      this.roomAdded.emit(response.result);
      if (this.handsetRoomDialogRef?.close) {
        this.handsetRoomDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }
}
