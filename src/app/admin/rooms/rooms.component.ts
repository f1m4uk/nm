import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, switchMapTo } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subject, Subscription, combineLatest } from 'rxjs';
import { TranslocoService } from '@ngneat/transloco';
import { RoomsService } from 'src/app/rooms/rooms.service';
import { Room } from 'src/app/rooms/room.model';
import { HandsetRoomDialogComponent } from './handset-room-dialog/handset-room-dialog.component';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetRoomDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  roomsSubject = new BehaviorSubject<Room[]>([]);
  rooms$: Observable<Room[]> = this.roomsSubject.pipe(
    switchMapTo(this.roomsService.getRooms())
  );
  selectedRoomSubject = new Subject<Room>();
  selectedRoom$: Observable<Room> = this.selectedRoomSubject.asObservable();

  selectedRoomOnDevice: Subscription;

  window: Window = window;

  constructor(
    private roomsService: RoomsService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedRoomOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedRoomOnDevice = combineLatest(this.isHandset$, this.selectedRoom$).subscribe(info => {
      const isHandset = info[0];
      const selectedRoom = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetRoomDialogComponent, {
          id: 'handset-room-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            room: selectedRoom
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.roomsSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }
}
