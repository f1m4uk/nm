import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetRoomDialogComponent } from './handset-room-dialog.component';

describe('HandsetRoomDialogComponent', () => {
  let component: HandsetRoomDialogComponent;
  let fixture: ComponentFixture<HandsetRoomDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetRoomDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetRoomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
