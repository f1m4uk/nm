import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from 'src/app/rooms/room.model';
import { RoomsService } from 'src/app/rooms/rooms.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-room-dialog',
  templateUrl: './handset-room-dialog.component.html',
  styleUrls: ['./handset-room-dialog.component.css']
})
export class HandsetRoomDialogComponent implements OnInit {
  room$: Observable<Room> = this.data.room ? this.roomsService.getRoom(this.data.room._id) : null;

  constructor(
    private roomsService: RoomsService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetRoomDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }

}
