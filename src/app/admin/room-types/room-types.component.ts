import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, Subject, Subscription, combineLatest } from 'rxjs';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, switchMapTo } from 'rxjs/operators';
import { RoomType } from 'src/app/room-types/room-type.model';
import { TranslocoService } from '@ngneat/transloco';
import { RoomTypesService } from 'src/app/room-types/room-types.service';
import { HandsetRoomTypeDialogComponent } from './handset-room-type-dialog/handset-room-type-dialog.component';

@Component({
  selector: 'app-admin-room-types',
  templateUrl: './room-types.component.html',
  styleUrls: ['./room-types.component.css']
})
export class AdminRoomTypesComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetRoomTypeDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  roomTypesSubject = new BehaviorSubject<RoomType[]>([]);
  roomTypes$: Observable<RoomType[]> = this.roomTypesSubject.pipe(
    switchMapTo(this.roomTypesService.getRoomTypes())
  );
  selectedRoomTypeSubject = new Subject<RoomType>();
  selectedRoomType$: Observable<RoomType> = this.selectedRoomTypeSubject.asObservable();

  selectedRoomTypeOnDevice: Subscription;

  window: Window = window;

  constructor(
    private roomTypesService: RoomTypesService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedRoomTypeOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedRoomTypeOnDevice = combineLatest(this.isHandset$, this.selectedRoomType$).subscribe(info => {
      const isHandset = info[0];
      const selectedRoomType = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetRoomTypeDialogComponent, {
          id: 'handset-room-type-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            roomType: selectedRoomType
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.roomTypesSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }
}
