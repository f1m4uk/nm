import { Component, OnInit, OnChanges, OnDestroy, ViewChild, ElementRef, EventEmitter, Output, Input, SimpleChanges } from '@angular/core';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Observable, Subscription, BehaviorSubject, combineLatest, of } from 'rxjs';
import { RoomTypesService } from 'src/app/room-types/room-types.service';
import { TranslocoService } from '@ngneat/transloco';
import { HandsetRoomTypeDialogComponent } from '../handset-room-type-dialog/handset-room-type-dialog.component';
import { MatDialogRef } from '@angular/material/dialog';
import { RoomType } from 'src/app/room-types/room-type.model';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { mimeType } from 'src/app/mime-type.validator';
import { Service } from 'src/app/services/service.model';
import { ServicesService } from 'src/app/services/services.service';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';

@Component({
  selector: 'app-room-type',
  templateUrl: './room-type.component.html',
  styleUrls: ['./room-type.component.css']
})
export class RoomTypeComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  form = new FormGroup({
    name_en: new FormControl(''),
    name_ru: new FormControl(null, {validators: [Validators.required]}),
    name_uk: new FormControl(''),
    description_en: new FormControl(''),
    description_ru: new FormControl(null, {validators: [Validators.required]}),
    description_uk: new FormControl(''),
    servicesIds: new FormControl(),
    images: new FormControl([], { asyncValidators: [mimeType] }),
    imagePaths: new FormControl()
  });

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;
  imagesSubject = new BehaviorSubject<{file: File, url: string | ArrayBuffer}[]>([]);
  imageURLs$: Observable<(string | ArrayBuffer)[]> = this.imagesSubject.asObservable().pipe(
    tap(images => {
      this.form.patchValue({images: images.map(image => image.file)});
      this.form.get('images').updateValueAndValidity();
    }),
    map(images => images.map(image => image.url))
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  roomServices$: Observable<Service[]> = this.servicesService.getRoomsServices().pipe(shareReplay());
  @Input() roomTypeId: string;

  roomType$: Observable<RoomType>;

  @Output() roomTypeAdded = new EventEmitter<RoomType>();
  roomTypeByLanguageSubscription: Subscription;
  @Output() roomTypeDeleted = new EventEmitter<string>();
  selectedServicesSubject = new BehaviorSubject<Service[]>([]);
  selectedServices$: Observable<Service[]> = this.selectedServicesSubject.asObservable();
  @ViewChild('servicesListAutocompleteTrigger', { read: ElementRef }) servicesListAutocompleteTriggerRef: ElementRef<HTMLInputElement>;

  constructor(
    private roomTypesService: RoomTypesService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService,
    private servicesService: ServicesService,
    public handsetRoomTypeDialogRef: MatDialogRef<HandsetRoomTypeDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.roomTypeId) {
      if (changes.roomTypeId.currentValue !== null) {
        this.roomType$ = this.roomTypesService.getRoomType(changes.roomTypeId.currentValue).pipe(tap(roomType => {
          this.form.patchValue(roomType);
          this.form.patchValue({
            servicesIds: roomType.services.map(service => service._id)
          });
          this.selectedServicesSubject.next(roomType.services);
        }), shareReplay());
        this.roomTypeByLanguageSubscription = combineLatest(this.roomType$, this.language$).subscribe(result => {
          const roomType = result[0];
          if (this.roomType$ !== null) {
            this.form.patchValue(roomType);
            this.form.patchValue({
              servicesIds: roomType.services.map(service => service._id)
            });
          }
        });
      } else {
        this.roomType$ = null;
        this.selectedServicesSubject.next([]);
        this.form.reset();
      }
      this.imagesSubject.next([]);
      if (this.filePickerRef?.nativeElement) {
        this.filePickerRef.nativeElement.value = '';
      }
    }
  }

  ngOnDestroy(): void {
    this.roomTypeByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onImagesPicked(event) {
    const files = (event.target as HTMLInputElement).files;
    if (files && files[0]) {
      for (let i = 0; i < files.length; i++) {
        const fileReader = new FileReader();

        fileReader.onload = () => {
          const images = this.imagesSubject.getValue();

          images.push({
            file: files[i],
            url: fileReader.result
          });

          this.imagesSubject.next(images);
        }

        fileReader.readAsDataURL(files[i]);
      }
    }
  }

  onImagePathRemoved(imagePath: string) {
    const removedImageIndexInNewImages = this.imagesSubject.getValue().findIndex(image => image.url === imagePath);
    if (removedImageIndexInNewImages >= 0) {
      const images = this.imagesSubject.getValue();
      images.splice(removedImageIndexInNewImages, 1);
      this.imagesSubject.next(images);
      return;
    }
    const removedImageIndexInOldImages = this.form.value.imagePaths.findIndex(path => path === imagePath);
    if (removedImageIndexInOldImages >= 0) {
      const imagePaths = this.form.value.imagePaths;
      imagePaths.splice(removedImageIndexInOldImages, 1);
      this.form.patchValue({imagePaths: imagePaths});
      return;
    }
  }

  onRoomTypeDelete() {
    if (this.roomType$) {
      this.isLoading = true;
      this.roomTypesService.deleteRoomType(this.roomTypeId).subscribe(result => {
        this.isLoading = false;
        this.roomTypeDeleted.emit(this.roomTypeId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onServiceRemove(index: number) {
    const selectedServices = this.selectedServicesSubject.getValue();
    selectedServices.splice(index, 1);
    this.selectedServicesSubject.next(selectedServices);
    this.form.patchValue({servicesIds: selectedServices.map(service => service._id)});
  }

  onSubmitRoomType() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newRoomType$: Observable<{message: string, result: RoomType}>;
    if (this.roomType$) {
      newRoomType$ = this.roomTypesService.updateRoomType(this.roomTypeId, this.form.value);
    } else {
      newRoomType$ = this.roomTypesService.createRoomType(this.form.value);
    }
    newRoomType$.subscribe(response => {
      this.isLoading = false;
      this.roomTypeAdded.emit(response.result);
      if (this.handsetRoomTypeDialogRef?.close) {
        this.handsetRoomTypeDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }

  serviceSelected(event: MatAutocompleteSelectedEvent): void {
    const selectedServices = this.selectedServicesSubject.getValue();
    selectedServices.push(event.option.value);
    this.selectedServicesSubject.next(selectedServices);
    this.form.patchValue({servicesIds: selectedServices.map(service => service._id)});
    if (this.servicesListAutocompleteTriggerRef?.nativeElement) {
      this.servicesListAutocompleteTriggerRef.nativeElement.value = '';
    }
  }
}
