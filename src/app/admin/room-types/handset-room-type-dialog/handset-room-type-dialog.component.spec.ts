import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetRoomTypeDialogComponent } from './handset-room-type-dialog.component';

describe('HandsetRoomTypeDialogComponent', () => {
  let component: HandsetRoomTypeDialogComponent;
  let fixture: ComponentFixture<HandsetRoomTypeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetRoomTypeDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetRoomTypeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
