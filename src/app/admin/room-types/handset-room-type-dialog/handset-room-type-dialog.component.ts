import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { RoomType } from 'src/app/room-types/room-type.model';
import { RoomTypesService } from 'src/app/room-types/room-types.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-room-type-dialog',
  templateUrl: './handset-room-type-dialog.component.html',
  styleUrls: ['./handset-room-type-dialog.component.css']
})
export class HandsetRoomTypeDialogComponent implements OnInit {
  roomType$: Observable<RoomType> = this.data.roomType ? this.roomTypesService.getRoomType(this.data.roomType._id) : null;

  constructor(
    private roomTypesService: RoomTypesService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetRoomTypeDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }

}
