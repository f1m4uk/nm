import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CancellationApprovalBottomSheetComponent } from './cancellation-approval-bottom-sheet.component';

describe('CancellationApprovalBottomSheetComponent', () => {
  let component: CancellationApprovalBottomSheetComponent;
  let fixture: ComponentFixture<CancellationApprovalBottomSheetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CancellationApprovalBottomSheetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CancellationApprovalBottomSheetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
