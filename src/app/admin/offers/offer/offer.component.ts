import { Component, OnInit, OnDestroy, OnChanges, Input, EventEmitter, Output, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';
import { Offer } from 'src/app/offers/offer.model';
import { OffersService } from 'src/app/offers/offers.service';
import { TranslocoService } from '@ngneat/transloco';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetOfferDialogComponent } from '../handset-offer-dialog/handset-offer-dialog.component';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit, OnChanges, OnDestroy {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  form = new FormGroup({
    icon: new FormControl(),
    name_en: new FormControl(),
    name_ru: new FormControl(null, {validators: [Validators.required]}),
    name_uk: new FormControl(),
    description_en: new FormControl(),
    description_ru: new FormControl(null, {validators: [Validators.required]}),
    description_uk: new FormControl()
  });
  @Input() offerId: string;

  offer$: Observable<Offer>;

  @Output() offerAdded = new EventEmitter<Offer>();
  offerByLanguageSubscription: Subscription;
  @Output() offerDeleted = new EventEmitter<string>();

  constructor(
    private offersService: OffersService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService,
    public handsetOfferDialogRef: MatDialogRef<HandsetOfferDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.offerId) {
      if (changes.offerId.currentValue !== null) {
        this.offer$ = this.offersService.getOffer(changes.offerId.currentValue).pipe(tap(offer => {
          this.form.patchValue(offer);
        }), shareReplay());
        this.offerByLanguageSubscription = combineLatest(this.offer$, this.language$).subscribe(result => {
          const offer = result[0];
          if (this.offer$ !== null) {
            this.form.patchValue(offer);
          }
        });
      } else {
        this.offer$ = null;
        this.form.reset();
      }
    }
  }

  ngOnDestroy(): void {
    this.offerByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onOfferDelete() {
    if (this.offer$) {
      this.isLoading = true;
      this.offersService.deleteOffer(this.offerId).subscribe(result => {
        this.isLoading = false;
        this.offerDeleted.emit(this.offerId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onSubmitOffer() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newOffer$: Observable<{message: string, result: Offer}>;
    if (this.offer$) {
      newOffer$ = this.offersService.updateOffer(this.offerId, this.form.value);
    } else {
      newOffer$ = this.offersService.createOffer(this.form.value);
    }
    newOffer$.subscribe(response => {
      this.isLoading = false;
      this.offerAdded.emit(response.result);
      if (this.handsetOfferDialogRef?.close) {
        this.handsetOfferDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }
}
