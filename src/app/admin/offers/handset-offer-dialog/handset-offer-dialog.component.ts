import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Offer } from 'src/app/offers/offer.model';
import { OffersService } from 'src/app/offers/offers.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-offer-dialog',
  templateUrl: './handset-offer-dialog.component.html',
  styleUrls: ['./handset-offer-dialog.component.css']
})
export class HandsetOfferDialogComponent implements OnInit {
  offer$: Observable<Offer> = this.data.offer ? this.offersService.getOffer(this.data.offer._id) : null;

  constructor(
    private offersService: OffersService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetOfferDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }
}
