import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetOfferDialogComponent } from './handset-offer-dialog.component';

describe('HandsetOfferDialogComponent', () => {
  let component: HandsetOfferDialogComponent;
  let fixture: ComponentFixture<HandsetOfferDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetOfferDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetOfferDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
