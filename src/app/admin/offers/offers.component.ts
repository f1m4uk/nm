import { Component, OnInit, OnDestroy } from '@angular/core';
import { HandsetOfferDialogComponent } from './handset-offer-dialog/handset-offer-dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Observable, BehaviorSubject, Subject, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, switchMapTo } from 'rxjs/operators';
import { Offer } from 'src/app/offers/offer.model';
import { OffersService } from 'src/app/offers/offers.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetOfferDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  offersSubject = new BehaviorSubject<Offer[]>([]);
  offers$: Observable<Offer[]> = this.offersSubject.pipe(
    switchMapTo(this.offersService.getOffers())
  );
  selectedOfferSubject = new Subject<Offer>();
  selectedOffer$: Observable<Offer> = this.selectedOfferSubject.asObservable();

  selectedOfferOnDevice: Subscription;

  window: Window = window;

  constructor(
    private offersService: OffersService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedOfferOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedOfferOnDevice = combineLatest(this.isHandset$, this.selectedOffer$).subscribe(info => {
      const isHandset = info[0];
      const selectedOffer = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetOfferDialogComponent, {
          id: 'handset-offer-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            offer: selectedOffer
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.offersSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }
}
