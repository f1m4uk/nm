import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { Service } from 'src/app/services/service.model';
import { ServicesService } from 'src/app/services/services.service';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-service-dialog',
  templateUrl: './handset-service-dialog.component.html',
  styleUrls: ['./handset-service-dialog.component.css']
})
export class HandsetServiceDialogComponent implements OnInit {
  service$: Observable<Service> = this.data.service ? this.servicesService.getService(this.data.service._id) : null;

  constructor(
    private servicesService: ServicesService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetServiceDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }

}
