import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetServiceDialogComponent } from './handset-service-dialog.component';

describe('HandsetServiceDialogComponent', () => {
  let component: HandsetServiceDialogComponent;
  let fixture: ComponentFixture<HandsetServiceDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetServiceDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetServiceDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
