import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Service } from 'src/app/services/service.model';
import { ServicesService } from 'src/app/services/services.service';
import { shareReplay, tap, map } from 'rxjs/operators';
import { Observable, BehaviorSubject, combineLatest, Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { mimeType } from 'src/app/mime-type.validator';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetServiceDialogComponent } from '../handset-service-dialog/handset-service-dialog.component';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;
  imagesSubject = new BehaviorSubject<{file: File, url: string | ArrayBuffer}[]>([]);
  imageURLs$: Observable<(string | ArrayBuffer)[]> = this.imagesSubject.asObservable().pipe(
    tap(images => {
      this.form.patchValue({images: images.map(image => image.file)});
      this.form.get('images').updateValueAndValidity();
    }),
    map(images => images.map(image => image.url))
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  form = new FormGroup({
    icon: new FormControl(''),
    name_en: new FormControl(''),
    name_ru: new FormControl('', {validators: [Validators.required]}),
    name_uk: new FormControl(''),
    shortDescription_en: new FormControl(''),
    shortDescription_ru: new FormControl('', {validators: [Validators.required]}),
    shortDescription_uk: new FormControl(''),
    fullDescription_en: new FormControl(''),
    fullDescription_ru: new FormControl(''),
    fullDescription_uk: new FormControl(''),
    isHotelService: new FormControl(false),
    isRoomService: new FormControl(false),
    isAdditionalService: new FormControl(false),
    images: new FormControl([], { asyncValidators: [mimeType] }),
    imagePaths: new FormControl()
  });
  @Input() serviceId: string;

  service$: Observable<Service>;

  @Output() serviceAdded = new EventEmitter<Service>();
  serviceByLanguageSubscription: Subscription;
  @Output() serviceDeleted = new EventEmitter<string>();

  constructor(
    private servicesService: ServicesService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService,
    public handsetServiceDialogRef: MatDialogRef<HandsetServiceDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.serviceId) {
      if (changes.serviceId.currentValue !== null) {
        this.service$ = this.servicesService.getService(changes.serviceId.currentValue).pipe(tap(service => {
          this.form.patchValue(service);
        }), shareReplay());
        this.serviceByLanguageSubscription = combineLatest(this.service$, this.language$).subscribe(result => {
          const service = result[0];
          if (this.service$ !== null) {
            this.form.patchValue(service);
          }
        });
      } else {
        this.service$ = null;
        this.form.reset();
      }
      this.imagesSubject.next([]);
      if (this.filePickerRef?.nativeElement) {
        this.filePickerRef.nativeElement.value = '';
      }
    }
  }

  ngOnDestroy(): void {
    this.serviceByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onImagesPicked(event) {
    const files = (event.target as HTMLInputElement).files;
    if (files && files[0]) {
      for (let i = 0; i < files.length; i++) {
        const fileReader = new FileReader();

        fileReader.onload = () => {
          const images = this.imagesSubject.getValue();

          images.push({
            file: files[i],
            url: fileReader.result
          });

          this.imagesSubject.next(images);
        }

        fileReader.readAsDataURL(files[i]);
      }
    }
  }

  onImagePathRemoved(imagePath: string) {
    const removedImageIndexInNewImages = this.imagesSubject.getValue().findIndex(image => image.url === imagePath);
    if (removedImageIndexInNewImages >= 0) {
      const images = this.imagesSubject.getValue();
      images.splice(removedImageIndexInNewImages, 1);
      this.imagesSubject.next(images);
      return;
    }
    const removedImageIndexInOldImages = this.form.value.imagePaths.findIndex(path => path === imagePath);
    if (removedImageIndexInOldImages >= 0) {
      const imagePaths = this.form.value.imagePaths;
      imagePaths.splice(removedImageIndexInOldImages, 1);
      this.form.patchValue({imagePaths: imagePaths});
      return;
    }
  }

  onServiceDelete() {
    if (this.service$) {
      this.isLoading = true;
      this.servicesService.deleteService(this.serviceId).subscribe(result => {
        this.isLoading = false;
        this.serviceDeleted.emit(this.serviceId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onSubmitService() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newService$: Observable<{message: string, result: Service}>;
    if (this.service$) {
      newService$ = this.servicesService.updateService(this.serviceId, this.form.value);
    } else {
      newService$ = this.servicesService.createService(this.form.value);
    }
    newService$.subscribe(response => {
      this.isLoading = false;
      this.serviceAdded.emit(response.result);
      if (this.handsetServiceDialogRef?.close) {
        this.handsetServiceDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }
}
