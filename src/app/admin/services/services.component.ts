import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';
import { Service } from 'src/app/services/service.model';
import { Observable, Subject, BehaviorSubject, Subscription, combineLatest } from 'rxjs';
import { shareReplay, map, switchMapTo } from 'rxjs/operators';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HandsetServiceDialogComponent } from './handset-service-dialog/handset-service-dialog.component';
import { TranslocoService } from '@ngneat/transloco';
import { MatListItemDirective } from 'src/app/mat-list-item.directive';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetServiceDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
    );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());
  @ViewChild(MatListItemDirective) matListItemDirective: MatListItemDirective;

  servicesSubject = new BehaviorSubject<Service[]>([]);
  services$: Observable<Service[]> = this.servicesSubject.pipe(
    switchMapTo(this.servicesService.getServices())
  );
  selectedServiceSubject = new Subject<Service>();
  selectedService$: Observable<Service> = this.selectedServiceSubject.asObservable();

  selectedServiceOnDevice: Subscription;

  window: Window = window;

  constructor(
    private servicesService: ServicesService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedServiceOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedServiceOnDevice = combineLatest(this.isHandset$, this.selectedService$).subscribe(info => {
      const isHandset = info[0];
      const selectedService = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetServiceDialogComponent, {
          id: 'handset-service-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            service: selectedService
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.servicesSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }

  onDeleteButtonClick(event: Event, service: Service) {
    // https://github.com/angular/components/issues/6398
    event.stopImmediatePropagation();
    const target = event.target as HTMLElement;
    this.matListItemDirective.resetElement(target.closest('mat-list-item'));
    this.servicesService.deleteService(service._id);
  }
}
