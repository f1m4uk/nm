import { Component, OnInit } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  isBackdropCollapsed = true;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  selectedSectionSubject = new BehaviorSubject<string>(null);
  selectedSection$ = this.selectedSectionSubject.asObservable().pipe(
    tap(selectedSection => {
      if (selectedSection) {
        this.isBackdropCollapsed = false;
      }
    })
  );

  constructor(
    private breakpointObserver: BreakpointObserver
  ) { }

  ngOnInit(): void {}
}
