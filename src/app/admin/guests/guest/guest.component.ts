import { Component, OnInit, OnDestroy, OnChanges, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Guest } from 'src/app/guests/guest.model';
import { GuestsService } from 'src/app/guests/guests.service';
import { TranslocoService } from '@ngneat/transloco';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetGuestDialogComponent } from '../handset-guest-dialog/handset-guest-dialog.component';
import { Room } from 'src/app/rooms/room.model';
import { RoomsService } from 'src/app/rooms/rooms.service';
import { DateAdapter } from '@angular/material/core';
import { MomentService } from 'src/app/moment.service';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.css']
})
export class GuestComponent implements OnInit, OnChanges, OnDestroy {
  form = new FormGroup({
    startDate: new FormControl(null, {validators: [Validators.required]}),
    endDate: new FormControl(null, {validators: [Validators.required]}),
    phoneNumber: new FormControl(null, {validators: [Validators.required, Validators.minLength(10), Validators.maxLength(10)]}),
    peopleAmount: new FormControl(null, {validators: [Validators.required]}),
    roomId: new FormControl(null, {validators: [Validators.required]}),
    notes: new FormControl(),
    isReservation: new FormControl()
  });

  @Input() guestId: string;

  guest$: Observable<Guest>;

  @Output() guestAdded = new EventEmitter<Guest>();
  guestByLanguageSubscription: Subscription;
  @Output() guestDeleted = new EventEmitter<string>();

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;

  language$: Observable<string> = this.translocoService.langChanges$.pipe(tap(language => this.dateAdapter.setLocale(language)), shareReplay());

  rooms$: Observable<Room[]> = this.roomsService.getRooms().pipe(shareReplay());

  constructor(
    private breakpointObserver: BreakpointObserver,
    private dateAdapter: DateAdapter<any>,
    private guestsService: GuestsService,
    private momentService: MomentService,
    private roomsService: RoomsService,
    private translocoService: TranslocoService,
    public handsetGuestDialogRef: MatDialogRef<HandsetGuestDialogComponent>,
  ) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.guestId) {
      if (changes.guestId.currentValue !== null) {
        this.guest$ = this.guestsService.getGuest(changes.guestId.currentValue).pipe(tap(guest => {
          guest.startDate = this.momentService.getMoment()(guest.startDate);
          guest.endDate = this.momentService.getMoment()(guest.endDate);
          guest.phoneNumber = guest.phoneNumber.replace('+380', '');
          this.form.patchValue(guest);
          this.form.patchValue({
            roomId: guest.room._id
          });
        }), shareReplay());
        this.guestByLanguageSubscription = combineLatest(this.guest$, this.language$).subscribe(result => {
          const guest = result[0];
          if (this.guest$ !== null) {
            guest.startDate = this.momentService.getMoment()(guest.startDate);
            guest.endDate = this.momentService.getMoment()(guest.endDate);
            guest.phoneNumber = guest.phoneNumber.replace('+380', '');
            this.form.patchValue(guest);
            this.form.patchValue({
              roomId: guest.room._id
            });
          }
        });
      } else {
        this.guest$ = null;
        this.form.reset();
      }
    }
  }

  ngOnDestroy(): void {
    this.guestByLanguageSubscription?.unsubscribe();
  }

  ngOnInit(): void {
  }

  onGuestDelete() {
    if (this.guest$) {
      this.isLoading = true;
      this.guestsService.deleteGuest(this.guestId).subscribe(result => {
        this.isLoading = false;
        this.guestDeleted.emit(this.guestId);
      }, error => {
        this.isLoading = false;
      });
    }
  }

  onSubmitGuest() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    let newGuest$: Observable<{message: string, result: Guest}>;
    if (this.guest$) {
      newGuest$ = this.guestsService.updateGuest(this.guestId, this.form.value);
    } else {
      newGuest$ = this.guestsService.createGuest(this.form.value);
    }
    newGuest$.subscribe(response => {
      this.isLoading = false;
      this.guestAdded.emit(response.result);
      if (this.handsetGuestDialogRef?.close) {
        this.handsetGuestDialogRef.close();
      }
    }, error => {
      this.isLoading = false
    });
  }
}
