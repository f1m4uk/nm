import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetGuestDialogComponent } from './handset-guest-dialog.component';

describe('HandsetGuestDialogComponent', () => {
  let component: HandsetGuestDialogComponent;
  let fixture: ComponentFixture<HandsetGuestDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetGuestDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetGuestDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
