import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { Guest } from 'src/app/guests/guest.model';
import { GuestsService } from 'src/app/guests/guests.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { CancellationApprovalBottomSheetComponent } from '../../cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';

@Component({
  selector: 'app-handset-guest-dialog',
  templateUrl: './handset-guest-dialog.component.html',
  styleUrls: ['./handset-guest-dialog.component.css']
})
export class HandsetGuestDialogComponent implements OnInit {
  guest$: Observable<Guest> = this.data.guest ? this.guestsService.getGuest(this.data.guest._id) : null;

  constructor(
    private guestsService: GuestsService,
    @Inject(MAT_DIALOG_DATA) public data,
    private bottomSheet: MatBottomSheet,
    public dialogRef: MatDialogRef<HandsetGuestDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  openCancellationApproval() {
    this.bottomSheet.open(CancellationApprovalBottomSheetComponent).afterDismissed().toPromise().then(areChangesDiscarded => {
      if (areChangesDiscarded) {
        this.dialogRef.close(areChangesDiscarded);
      }
    });
  }
}
