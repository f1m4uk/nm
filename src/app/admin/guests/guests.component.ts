import { Component, OnInit, OnDestroy } from '@angular/core';
import { HandsetGuestDialogComponent } from './handset-guest-dialog/handset-guest-dialog.component';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Observable, BehaviorSubject, Subject, Subscription, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, switchMapTo } from 'rxjs/operators';
import { Guest } from 'src/app/guests/guest.model';
import { GuestsService } from 'src/app/guests/guests.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-guests',
  templateUrl: './guests.component.html',
  styleUrls: ['./guests.component.css']
})
export class GuestsComponent implements OnInit, OnDestroy {
  handsetDialog: MatDialogRef<HandsetGuestDialogComponent>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  guestsSubject = new BehaviorSubject<Guest[]>([]);
  guests$: Observable<Guest[]> = this.guestsSubject.pipe(
    switchMapTo(this.guestsService.getGuests())
  );
  selectedGuestSubject = new Subject<Guest>();
  selectedGuest$: Observable<Guest> = this.selectedGuestSubject.asObservable();

  selectedGuestOnDevice: Subscription;

  window: Window = window;

  constructor(
    private guestsService: GuestsService,
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.selectedGuestOnDevice.unsubscribe();
  }

  ngOnInit(): void {
    this.selectedGuestOnDevice = combineLatest(this.isHandset$, this.selectedGuest$).subscribe(info => {
      const isHandset = info[0];
      const selectedGuest = info[1];
      if (isHandset) {
        this.handsetDialog = this.matDialog.open(HandsetGuestDialogComponent, {
          id: 'handset-guest-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          height: '100%',
          width: '100%',
          panelClass: 'full-screen-dialog',
          data: {
            guest: selectedGuest
          }
        });
        this.handsetDialog.afterClosed().toPromise().then(areChangesDiscarded => {
          if (!areChangesDiscarded) {
            this.guestsSubject.next([]);
          }
        });
      } else {
        this.handsetDialog?.close();
      }
    });
  }
}
