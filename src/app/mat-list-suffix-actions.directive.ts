import { Directive } from '@angular/core';

@Directive({
  selector: '[appMatListSuffixActions]',
  host: {
    '[style.height]': '"100%"',
    '[style.position]': "'absolute'",
    '[style.right]': '0',
    '[style.display]': "'flex'",
    '[style.flex-direction]': "'row'",
    '[style.box-sizing]': "'border-box'",
    '[style.width]': '0'
  }
})
export class MatListSuffixActionsDirective {
  constructor() { }
}
