import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { OffersService } from '../offers.service';
import { TranslocoService } from '@ngneat/transloco';
import { Offer } from '../offer.model';

@Component({
  selector: 'app-offers-dialog',
  templateUrl: './offers-dialog.component.html',
  styleUrls: ['./offers-dialog.component.css']
})
export class OffersDialogComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  offers$: Observable<Offer[]> = this.offersService.getOffers().pipe(shareReplay());

  window: Window = window;

  constructor(
    private offersService: OffersService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService
  ) { }

  ngOnInit(): void {}
}
