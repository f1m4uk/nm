export interface Offer {
  _id: string,
  icon?: string,
  name_en?: string,
  name_ru: string,
  name_uk?: string,
  description_en?: string,
  description_ru: string,
  description_uk?: string
}
