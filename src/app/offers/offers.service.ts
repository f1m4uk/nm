import { Injectable } from '@angular/core';
import { Offer } from './offer.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const OFFERS_URL = environment.API_URL + '/offers';

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  constructor(private httpClient: HttpClient) { }

  createOffer(body: any): Observable<{message: string, result: Offer}> {
    return this.httpClient.post<{message: string, result: Offer}>(OFFERS_URL, body);
  }

  deleteOffer(id: string): Observable<Offer> {
    return this.httpClient.delete<Offer>(OFFERS_URL + '/' + id);
  }

  getOffers(): Observable<Offer[]> {
    return this.httpClient.get<Offer[]>(OFFERS_URL);
  }

  getOffer(id: string): Observable<Offer> {
    return this.httpClient.get<Offer>(OFFERS_URL + '/' + id);
  }

  updateOffer(id: string, body): Observable<{message: string, result: Offer}> {
    return this.httpClient.put<{message: string, result: Offer}>(OFFERS_URL + '/' + id, body);
  }
}
