import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslocoRootModule } from './transloco-root.module';
import { ContentLoaderModule } from '@netbasal/ngx-content-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { MainComponent } from './main/main.component';
import { BottomNavigationComponent } from './bottom-navigation/bottom-navigation.component';
import { TopFloatingToolbarComponent } from './top-floating-toolbar/top-floating-toolbar.component';
import { NavigationRailComponent } from './navigation-rail/navigation-rail.component';
import { ActionsRailComponent } from './actions-rail/actions-rail.component';
import { BackdropComponent } from './backdrop/backdrop.component';
import { CarouselComponent } from './carousel/carousel.component';
import { HotelComponent } from './hotel/hotel.component';
import { AdditionalServicesComponent } from './additional-services/additional-services.component';
import { HandsetCarouselComponent } from './handset-carousel/handset-carousel.component';
import { ProfileDialogComponent } from './profile-dialog/profile-dialog.component';
import { LanguagesMenuComponent } from './languages-menu/languages-menu.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { JwtModule } from "@auth0/angular-jwt";
import { ServicesComponent } from './admin/services/services.component';
import { ServiceComponent } from './admin/services/service/service.component';
import { HotelServiceComponent } from './hotel/hotel-service/hotel-service.component';
import { HandsetServiceDialogComponent } from './admin/services/handset-service-dialog/handset-service-dialog.component';
import { CancellationApprovalBottomSheetComponent } from './admin/cancellation-approval-bottom-sheet/cancellation-approval-bottom-sheet.component';
import { MatDialogRef } from '@angular/material/dialog';
import { HandsetRoomDialogComponent } from './admin/rooms/handset-room-dialog/handset-room-dialog.component';
import { RoomComponent } from './admin/rooms/room/room.component';
import { RoomTypesComponent } from './room-types/room-types.component';
import { AdminRoomTypesComponent } from './admin/room-types/room-types.component';
import { RoomServicesComponent } from './room-types/room-services/room-services.component';
import { RoomsComponent } from './admin/rooms/rooms.component';
import { HandsetRoomTypeDialogComponent } from './admin/room-types/handset-room-type-dialog/handset-room-type-dialog.component';
import { RoomTypeComponent } from './admin/room-types/room-type/room-type.component';
import { RulesComponent } from './admin/rules/rules.component';
import { RuleComponent } from './admin/rules/rule/rule.component';
import { HandsetRuleDialogComponent } from './admin/rules/handset-rule-dialog/handset-rule-dialog.component';
import { OffersComponent } from './admin/offers/offers.component';
import { OfferComponent } from './admin/offers/offer/offer.component';
import { HandsetOfferDialogComponent } from './admin/offers/handset-offer-dialog/handset-offer-dialog.component';
import { GuestsComponent } from './admin/guests/guests.component';
import { GuestComponent } from './admin/guests/guest/guest.component';
import { HandsetGuestDialogComponent } from './admin/guests/handset-guest-dialog/handset-guest-dialog.component';
import { RulesDialogComponent } from './rules/rules-dialog/rules-dialog.component';
import { OffersDialogComponent } from './offers/offers-dialog/offers-dialog.component';
import { ReviewsDialogComponent } from './reviews/reviews-dialog/reviews-dialog.component';
import { ErrorInterceptor } from './error.interceptor';
import { MatListSuffixActionsDirective } from './mat-list-suffix-actions.directive';
import { MatListItemDirective } from './mat-list-item.directive';
import { MatListPrefixActionsDirective } from './mat-list-prefix-actions.directive';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    BottomNavigationComponent,
    TopFloatingToolbarComponent,
    NavigationRailComponent,
    ActionsRailComponent,
    BackdropComponent,
    RoomServicesComponent,
    CarouselComponent,
    HotelComponent,
    AdditionalServicesComponent,
    HandsetCarouselComponent,
    ProfileDialogComponent,
    LanguagesMenuComponent,
    AdminComponent,
    ServicesComponent,
    ServiceComponent,
    HotelServiceComponent,
    HandsetServiceDialogComponent,
    CancellationApprovalBottomSheetComponent,
    HandsetRoomDialogComponent,
    RoomComponent,
    RoomTypesComponent,
    RoomsComponent,
    HandsetRoomTypeDialogComponent,
    RoomTypeComponent,
    AdminRoomTypesComponent,
    RulesComponent,
    RuleComponent,
    HandsetRuleDialogComponent,
    OffersComponent,
    OfferComponent,
    HandsetOfferDialogComponent,
    GuestsComponent,
    GuestComponent,
    HandsetGuestDialogComponent,
    RulesDialogComponent,
    OffersDialogComponent,
    ReviewsDialogComponent,
    MatListSuffixActionsDirective,
    MatListItemDirective,
    MatListPrefixActionsDirective
  ],
  imports: [
    BrowserModule,
    HammerModule,
    AppRoutingModule,
    HttpClientModule,
    TranslocoRootModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    ContentLoaderModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    ReactiveFormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => localStorage.getItem('token'),
        allowedDomains: ['localhost:3000', '10.0.1.4:3000', 'desolate-everglades-80255.herokuapp.com']
      }
    })
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProfileDialogComponent,
    HandsetServiceDialogComponent,
    HandsetRoomDialogComponent,
    HandsetRoomTypeDialogComponent,
    CancellationApprovalBottomSheetComponent
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class AppModule { }
