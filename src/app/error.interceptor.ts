import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslocoService } from '@ngneat/transloco';
import { Clipboard } from '@angular/cdk/clipboard';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(
    private clipboard: Clipboard,
    private matSnackBar: MatSnackBar,
    private translocoService: TranslocoService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(catchError((httpErrorResponse: HttpErrorResponse) => {
      if (httpErrorResponse.status !== 401) {
        this.matSnackBar.open(
          this.translocoService.translate("errors.handler.message"),
          this.translocoService.translate("errors.handler.actions.copy")
        ).onAction().toPromise().then(() => this.clipboard.copy(httpErrorResponse.error));
      }
      return throwError(httpErrorResponse);
    }));
  }
}
