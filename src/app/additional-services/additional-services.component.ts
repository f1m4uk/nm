import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { Service } from '../services/service.model';
import { ServicesService } from '../services/services.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-additional-services',
  templateUrl: './additional-services.component.html',
  styleUrls: ['./additional-services.component.css']
})
export class AdditionalServicesComponent implements OnInit {
  additionalServices$: Observable<Service[]> = this.servicesService.getAdditionalServices().pipe(shareReplay());

  additionalServicesSubscription: Subscription = this.additionalServices$.subscribe(additionalServices => this.selectedAdditionalService$ = this.servicesService.getService(additionalServices[0]._id).pipe(shareReplay()));

  isBackdropCollapsed = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  selectedAdditionalService$: Observable<Service>;

  window = window;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private servicesService: ServicesService,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.additionalServicesSubscription.unsubscribe();
  }

  ngOnInit(): void {
  }


  onAdditionalServiceSelect(additionalService: Service) {
    this.selectedAdditionalService$ = this.servicesService.getService(additionalService._id).pipe(shareReplay());
    this.isBackdropCollapsed = false;
  }
}
