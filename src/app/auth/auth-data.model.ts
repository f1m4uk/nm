export interface AuthData {
  phoneNumber: string,
  password?: string
}
