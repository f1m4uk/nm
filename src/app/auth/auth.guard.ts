import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProfileDialogComponent } from '../profile-dialog/profile-dialog.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private matDialog: MatDialog,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authService.isLoggedIn$.pipe(map(isLoggedIn => {
      if (!isLoggedIn) {
        const profileDialog: MatDialogRef<ProfileDialogComponent> = this.matDialog.open(ProfileDialogComponent, {
          id: 'profile-dialog',
          panelClass: 'relatively-positioned-dialog',
          data: {
            closeAfterLogin: true
          },
        });
        profileDialog.afterClosed().toPromise().then(event => {
          this.router.navigateByUrl(state.url);
        });
      }
      return isLoggedIn;
    }));
  }

}
