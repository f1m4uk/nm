import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthData } from './auth-data.model';
import { tap, shareReplay, first } from 'rxjs/operators';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

const AUTH_URL = environment.API_URL + '/users';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnDestroy {
  isLoggedInSubject = new BehaviorSubject<boolean>(!this.jwtHelper.isTokenExpired(localStorage.getItem('token')));
  isLoggedIn$: Observable<boolean> = this.isLoggedInSubject.asObservable();
  refreshSessionOnce: Subscription = this.isLoggedIn$.pipe(first()).subscribe(isLoggedIn => {
    if (isLoggedIn) {
      this.refreshSession();
    }
  });
  tokenSubject = new BehaviorSubject<string>(localStorage.getItem('token'));
  token$: Observable<string> = this.tokenSubject.asObservable().pipe(shareReplay());

  tokenTimeout: any;
  tokenTimeoutSubscription: Subscription = this.token$.subscribe(token => {
    if (token) {
      this.tokenTimeout = setTimeout(() => {
        this.logout();
      }, this.jwtHelper.getTokenExpirationDate(token).getTime() - new Date().getTime());
    } else {
      clearTimeout(this.tokenTimeout);
    }
  });


  constructor(
    private httpClient: HttpClient,
    private jwtHelper: JwtHelperService,
    private router: Router,
  ) { }

  login(phoneNumber: string, password?: string) {
    const authData: AuthData = {
      phoneNumber: phoneNumber,
      password: password
    }
    return this.httpClient.post<{token: string}>(AUTH_URL + '/login', authData).pipe(
      tap(response => {
        if (response.token) {
          this.isLoggedInSubject.next(true);
          localStorage.setItem('token', response.token);
          this.tokenSubject.next(response.token);
        } else {
          this.logout();
        }
      })
    );
  }

  logout(): void {
    localStorage.removeItem('token');
    this.isLoggedInSubject.next(false);
    this.tokenSubject.next(null);
    this.router.navigate(['/']);
  }

  ngOnDestroy(): void {
    this.tokenTimeoutSubscription.unsubscribe();
  }

  refreshSession() {
    console.log('here')
    this.httpClient.get<{token: string}>(AUTH_URL + '/current-user/refresh-session').toPromise().then(response => {
      this.isLoggedInSubject.next(true);
      localStorage.setItem('token', response.token);
      this.tokenSubject.next(response.token);
    });
  }
}
