import { Injectable, OnDestroy, OnInit } from '@angular/core';
import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';
import { TranslocoService } from '@ngneat/transloco';
import { Subscription } from 'rxjs';

const moment = _rollupMoment || _moment;

@Injectable({
  providedIn: 'root'
})
export class MomentService implements OnDestroy {

  languageChangeSubsription: Subscription = this.translocoService.langChanges$.subscribe(language => {
    moment.locale(language);
  });

  constructor(
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.languageChangeSubsription?.unsubscribe();
  }

  getMoment() {
    return moment;
  }
}
