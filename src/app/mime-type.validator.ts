import { AbstractControl } from '@angular/forms';
import { Observable, Observer, forkJoin, of } from 'rxjs';
import { map } from 'rxjs/operators';

export const mimeType = (
  control: AbstractControl
): Promise<{ [key: string]: any }> | Observable<{ [key: string]: any }> => {
  if (typeof(control.value) === 'string' || !control.value || control.value.length === 0) {
    return of(null);
  }
  let files = control.value;
  if (files instanceof File) {
    files = [files];
  }
  const observables = [];
  for (let i = 0; i < files.length; i++) {
    const fileReader = new FileReader();
    observables.push(Observable.create(
      (observer: Observer<{ [key: string]: any }>) => {
        fileReader.addEventListener("loadend", () => {
          const arr = new Uint8Array(fileReader.result as ArrayBuffer).subarray(0, 4);
          let header = "";
          let isValid = false;
          for (let i = 0; i < arr.length; i++) {
            header += arr[i].toString(16);
          }
          switch (header) {
            case "89504e47":
              isValid = true;
              break;
            case "ffd8ffe0":
            case "ffd8ffe1":
            case "ffd8ffe2":
            case "ffd8ffe3":
            case "ffd8ffe8":
              isValid = true;
              break;
            default:
              isValid = false; // Or you can use the blob.type as fallback
              break;
          }
          if (isValid) {
            observer.next(null);
          } else {
            observer.next({ invalidMimeType: true });
          }
          observer.complete();
        });
        fileReader.readAsArrayBuffer(files[i]);
      }
    ));
  }

  return forkJoin(observables).pipe(map(validationErrors => validationErrors.some(validationError => validationError !== null) ? { invalidMimeType: true } : null));
};
