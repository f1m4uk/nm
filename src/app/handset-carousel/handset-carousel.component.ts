import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-handset-carousel',
  templateUrl: './handset-carousel.component.html',
  styleUrls: ['./handset-carousel.component.css']
})
export class HandsetCarouselComponent implements OnInit {
  @Input() imagePaths: string[];

  window: Window = window;

  constructor() { }

  ngOnInit(): void {
  }

}
