import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandsetCarouselComponent } from './handset-carousel.component';

describe('HandsetCarouselComponent', () => {
  let component: HandsetCarouselComponent;
  let fixture: ComponentFixture<HandsetCarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandsetCarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandsetCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
