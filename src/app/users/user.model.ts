export interface User {
  _id: string,
  avatarUrl?: string,
  phoneNumber: string
}
