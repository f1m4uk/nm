import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './user.model';
import { environment } from 'src/environments/environment';
import { shareReplay, switchMapTo } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { GuestOfCurrentUser } from '../guests/guest-of-current-user.model';

const USERS_URL = environment.API_URL + '/users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  user$: Observable<User> = this.authService.isLoggedIn$.pipe(switchMapTo(this.getCurrentUser())).pipe(shareReplay());

  constructor(
    private authService: AuthService,
    private httpClient: HttpClient
  ) { }

  getCurrentUser(): Observable<User> {
    return this.httpClient.get<User>(USERS_URL + '/current-user');
  }

  getGuestsOfCurrentUser(): Observable<GuestOfCurrentUser[]> {
    return this.httpClient.get<GuestOfCurrentUser[]>(USERS_URL + '/current-user/guests');
  }
}
