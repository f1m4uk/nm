import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable, BehaviorSubject, combineLatest } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, tap, switchMapTo } from 'rxjs/operators';
import { Review } from '../review.model';
import { ReviewsService } from '../reviews.service';
import { TranslocoService } from '@ngneat/transloco';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { mimeType } from 'src/app/mime-type.validator';
import { MatDialogRef } from '@angular/material/dialog';
import { RoleService } from 'src/app/role/role.service';
import { AuthService } from 'src/app/auth/auth.service';
import { MomentService } from 'src/app/moment.service';
import { User } from 'src/app/users/user.model';
import { UsersService } from 'src/app/users/users.service';

@Component({
  selector: 'app-reviews-dialog',
  templateUrl: './reviews-dialog.component.html',
  styleUrls: ['./reviews-dialog.component.css']
})
export class ReviewsDialogComponent implements OnInit {
  @ViewChild('filePicker') filePickerRef: ElementRef<HTMLInputElement>;
  form = new FormGroup({
    authorName: new FormControl(null, {validators: [Validators.required]}),
    description: new FormControl(''),
    image: new FormControl([], { asyncValidators: [mimeType] }),
    mark: new FormControl(null, {validators: [Validators.required]})
  });
  imageURL: string | ArrayBuffer = '';
  isAdmin$: Observable<boolean> = this.roleService.isAdmin$;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;
  isLoggedIn$ = this.authService.isLoggedIn$.pipe(shareReplay());

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());
  markSubject = new BehaviorSubject<number | null>(null);
  mark$: Observable<number | null> = this.markSubject.asObservable().pipe(
    tap(mark => {
      this.form.patchValue({
        mark: mark
      });
    }),
    shareReplay()
  );

  reviewsUnderReplySubject = new BehaviorSubject<string[]>([]);
  reviewsUnderReply$ = this.reviewsUnderReplySubject.asObservable().pipe(shareReplay());
  loadingReviewsSubject = new BehaviorSubject<string[]>([]);
  loadingReviews$ = this.loadingReviewsSubject.asObservable().pipe(shareReplay());
  reviewsSubject = new BehaviorSubject<Review[]>([]);
  reviews$: Observable<Review[]> = this.reviewsSubject.pipe(switchMapTo(this.reviewsService.getReviews()), map(reviews => reviews.map(review => {
    review.date = this.momentService.getMoment()(review.date);
    return review;
  })), shareReplay());

  user$: Observable<User> = this.usersService.user$.pipe(shareReplay());
  userWroteReview$: Observable<boolean> = combineLatest(this.reviews$, this.user$).pipe(map(result => result[0].some(review => review.user._id === result[1]._id)), shareReplay());

  window: Window = window;

  constructor(
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver,
    private momentService: MomentService,
    private reviewsService: ReviewsService,
    private roleService: RoleService,
    private translocoService: TranslocoService,
    private usersService: UsersService,
    public dialogRef: MatDialogRef<ReviewsDialogComponent>
  ) { }

  ngOnInit(): void {}

  onHeaderActionClick() {
    this.markSubject.getValue() === null ? this.dialogRef.close() : this.markSubject.next(null);
  }

  onImagePicked(event) {
    const file = (event.target as HTMLInputElement).files[0];
    const fileReader = new FileReader();

    fileReader.onload = () => {
      this.form.patchValue({image: file});
      this.form.get('image').updateValueAndValidity();
      this.imageURL = fileReader.result;
    }

    fileReader.readAsDataURL(file);
  }

  onReviewAddToReply(review: Review) {
    const reviewsUnderReply = this.reviewsUnderReplySubject.getValue();
    reviewsUnderReply.push(review._id);
    this.reviewsUnderReplySubject.next(reviewsUnderReply);
  }

  onReviewApprovalToggle(review: Review) {
    const loadingReviews = this.loadingReviewsSubject.getValue();
    loadingReviews.push(review._id);
    this.loadingReviewsSubject.next(loadingReviews);

    const onComplete = () => {
      const loadingReviews = this.loadingReviewsSubject.getValue();
      const loadingReviewIndex = loadingReviews.findIndex(loadingReview => loadingReview === review._id);
      if (loadingReviewIndex >= 0) {
        loadingReviews.splice(loadingReviewIndex, 1);
        this.loadingReviewsSubject.next(loadingReviews);
      }
    }

    this.reviewsService.updateReview(review._id, {
      isApproved: !review.isApproved
    }).subscribe(response => {
      review.isApproved = response.result.isApproved;
      onComplete();
    }, onComplete);
  }

  onReviewReply(review: Review) {
    const loadingReviews = this.loadingReviewsSubject.getValue();
    if (loadingReviews.includes(review._id)) {
      return;
    }
    loadingReviews.push(review._id);
    this.loadingReviewsSubject.next(loadingReviews);

    const onComplete = () => {
      const reviewsUnderReply = this.reviewsUnderReplySubject.getValue();
      const reviewUnderReplyIndex = reviewsUnderReply.findIndex(reviewUnderReply => reviewUnderReply === review._id);
      if (reviewUnderReplyIndex >= 0) {
        reviewsUnderReply.splice(reviewUnderReplyIndex, 1);
        this.reviewsUnderReplySubject.next(reviewsUnderReply);
      }
      const loadingReviews = this.loadingReviewsSubject.getValue();
      const loadingReviewIndex = loadingReviews.findIndex(loadingReview => loadingReview === review._id);
      if (loadingReviewIndex >= 0) {
        loadingReviews.splice(loadingReviewIndex, 1);
        this.loadingReviewsSubject.next(loadingReviews);
      }
    }

    this.reviewsService.updateReview(review._id, {
      adminNotes: review.adminNotes
    }).subscribe(onComplete, error => {
      const loadingReviews = this.loadingReviewsSubject.getValue();
      const loadingReviewIndex = loadingReviews.findIndex(loadingReview => loadingReview === review._id);
      if (loadingReviewIndex >= 0) {
        loadingReviews.splice(loadingReviewIndex, 1);
        this.loadingReviewsSubject.next(loadingReviews);
      }
    });
  }

  onSubmitReview() {
    if (!this.form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.reviewsService.createReview(this.form.value).subscribe(response => {
      this.isLoading = false;
      this.markSubject.next(null);
      this.reviewsSubject.next([]);
    }, error => {
      this.isLoading = false;
    })
  }
}
