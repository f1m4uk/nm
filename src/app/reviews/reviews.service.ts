import { Injectable } from '@angular/core';
import { Review } from './review.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const REVIEWS_URL = environment.API_URL + '/reviews';

@Injectable({
  providedIn: 'root'
})
export class ReviewsService {

  constructor(private httpClient: HttpClient) { }

  createReview(body: any): Observable<{message: string, result: Review}> {
    const formData = new FormData();
    for (let field in body) {
      const value = body[field];
      if (Array.isArray(value)) {
        for (let valueField of value) {
          if (valueField !== null) {
            formData.append(field, valueField);
          }
        }
      } else {
        if (value !== null) {
          formData.append(field, value);
        }
      }
    }
    return this.httpClient.post<{message: string, result: Review}>(REVIEWS_URL, formData);
  }

  getReviews(): Observable<Review[]> {
    return this.httpClient.get<Review[]>(REVIEWS_URL);
  }

  updateReview(id: string, body): Observable<{message: string, result: Review}> {
    return this.httpClient.put<{message: string, result: Review}>(REVIEWS_URL + '/' + id, body);
  }
}
