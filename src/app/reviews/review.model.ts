import { User } from '../users/user.model';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';

export interface Review {
  _id: string,
  user: User,
  authorName: string,
  description?: string,
  imagePath?: string,
  mark: number,
  isApproved?: boolean,
  adminNotes?: string,
  date: _rollupMoment.Moment
}
