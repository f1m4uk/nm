import { Injectable } from '@angular/core';
import { Guest } from './guest.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const GUESTS_URL = environment.API_URL + '/guests';

@Injectable({
  providedIn: 'root'
})
export class GuestsService {

  constructor(private httpClient: HttpClient) { }

  createGuest(body: any): Observable<{message: string, result: Guest}> {
    return this.httpClient.post<{message: string, result: Guest}>(GUESTS_URL, body);
  }

  deleteGuest(id: string): Observable<Guest> {
    return this.httpClient.delete<Guest>(GUESTS_URL + '/' + id);
  }

  getGuests(): Observable<Guest[]> {
    return this.httpClient.get<Guest[]>(GUESTS_URL);
  }

  getGuest(id: string): Observable<Guest> {
    return this.httpClient.get<Guest>(GUESTS_URL + '/' + id);
  }

  updateGuest(id: string, body): Observable<{message: string, result: Guest}> {
    return this.httpClient.put<{message: string, result: Guest}>(GUESTS_URL + '/' + id, body);
  }
}
