import { Room } from '../rooms/room.model';

import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';

export interface Guest {
  _id: string,
  phoneNumber: string,
  peopleAmount: number,
  room: Room,
  notes?: string,
  startDate: _rollupMoment.Moment,
  endDate: _rollupMoment.Moment,
  isReservation?: boolean
}
