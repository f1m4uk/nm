import * as _moment from 'moment';
// tslint:disable-next-line:no-duplicate-imports
import {default as _rollupMoment} from 'moment';

export interface GuestOfCurrentUser {
  peopleAmount: number,
  startDate: _rollupMoment.Moment,
  endDate: _rollupMoment.Moment,
}
