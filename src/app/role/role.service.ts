import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { AuthService } from '../auth/auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  isAdmin$ = this.authService.token$.pipe(map(token => this.jwtHelperService.decodeToken(token)?.role === 'admin'));

  constructor(
    private jwtHelperService: JwtHelperService,
    private authService: AuthService
  ) { }
}
