import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { shareReplay, map, switchMap, switchMapTo } from 'rxjs/operators';
import { Subscription, Observable, combineLatest, Subject, of } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { User } from '../users/user.model';
import { RulesDialogComponent } from '../rules/rules-dialog/rules-dialog.component';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { OffersDialogComponent } from '../offers/offers-dialog/offers-dialog.component';
import { ReviewsDialogComponent } from '../reviews/reviews-dialog/reviews-dialog.component';
import { SwUpdate } from '@angular/service-worker';
import { UsersService } from '../users/users.service';
import { MomentService } from '../moment.service';
import { environment } from 'src/environments/environment';
import { GuestOfCurrentUser } from '../guests/guest-of-current-user.model';

@Component({
  selector: 'app-profile-dialog',
  templateUrl: './profile-dialog.component.html',
  styleUrls: ['./profile-dialog.component.css']
})
export class ProfileDialogComponent implements OnInit, OnDestroy {
  adminPhone: string = environment.ADMIN_PHONE_NUMBER.replace('+380', '');
  guests$: Observable<GuestOfCurrentUser[]> = this.usersService.user$.pipe(switchMapTo(this.usersService.getGuestsOfCurrentUser()), map(guests => {
    return guests.map(guest => {
      guest.startDate = this.momentService.getMoment()(guest.startDate);
      guest.endDate = this.momentService.getMoment()(guest.endDate);
      return guest;
    });
  }), shareReplay());

  guest$: Observable<GuestOfCurrentUser> = this.guests$.pipe(map(guests => {
    return guests.find(guest => this.momentService.getMoment()(new Date()).isBetween(guest.startDate, guest.endDate));
  }));

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isLoading = false;
  isLoggedIn$ = this.authService.isLoggedIn$.pipe(shareReplay());
  isSystemUpdateAvailable$: Observable<boolean> = this.swUpdate.isEnabled ? this.swUpdate.available.pipe(map(event => true)) : of(false);
  loginSubscription: Subscription;

  offersDialog: MatDialogRef<OffersDialogComponent>;
  offersDialogOnDeviceSubscription: Subscription;
  offersDialogSubject = new Subject();

  reviewsDialog: MatDialogRef<ReviewsDialogComponent>;
  reviewsDialogOnDeviceSubscription: Subscription;
  reviewsDialogSubject = new Subject();

  rulesDialog: MatDialogRef<RulesDialogComponent>;
  rulesDialogOnDeviceSubscription: Subscription;
  rulesDialogSubject = new Subject();

  user$: Observable<User> = this.usersService.user$;

  window: Window = window;

  constructor(
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver,
    private dialogRef: MatDialogRef<ProfileDialogComponent>,
    private matDialog: MatDialog,
    private momentService: MomentService,
    private swUpdate: SwUpdate,
    private usersService: UsersService,
    @Inject(MAT_DIALOG_DATA) public data,
  ) { }


  initOffersDialogSubscription() {
    this.offersDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.offersDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const offersDialogSubjectValue = result[1];
      if (!offersDialogSubjectValue) {
        return;
      }
      const dialogWidth = 560;
      if (!this.offersDialog?.componentInstance) {
        this.offersDialog = this.matDialog.open(OffersDialogComponent, {
          panelClass: 'full-screen-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.offersDialog.updateSize('100%', '100%');
      } else {
        this.offersDialog.updateSize(dialogWidth + 'px');
      }
      this.offersDialog.afterClosed().toPromise().then(result => this.offersDialogSubject.next(false));
    });
  }

  initReviewsDialogSubscription() {
    this.reviewsDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.reviewsDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const reviewsDialogSubjectValue = result[1];
      if (!reviewsDialogSubjectValue) {
        return;
      }
      const dialogWidth = 560;
      if (!this.reviewsDialog?.componentInstance) {
        this.reviewsDialog = this.matDialog.open(ReviewsDialogComponent, {
          panelClass: ['full-screen-dialog', 'relatively-positioned-dialog', 'no-scroll-x'],
          maxWidth: '100vw',
          maxHeight: '100vh',
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.reviewsDialog.updateSize('100%', '100%');
      } else {
        this.reviewsDialog.updateSize(dialogWidth + 'px');
      }
      this.reviewsDialog.afterClosed().toPromise().then(result => this.reviewsDialogSubject.next(false));
    });
  }

  initRulesDialogSubscription() {
    this.rulesDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.rulesDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const rulesDialogSubjectValue = result[1];
      if (!rulesDialogSubjectValue) {
        return;
      }
      const dialogWidth = 560;
      if (!this.rulesDialog?.componentInstance) {
        this.rulesDialog = this.matDialog.open(RulesDialogComponent, {
          panelClass: 'full-screen-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.rulesDialog.updateSize('100%', '100%');
      } else {
        this.rulesDialog.updateSize(dialogWidth + 'px');
      }
      this.rulesDialog.afterClosed().toPromise().then(result => this.rulesDialogSubject.next(false));
    });
  }

  ngOnDestroy(): void {
    this.loginSubscription?.unsubscribe();
    this.rulesDialogOnDeviceSubscription?.unsubscribe();
    this.offersDialogOnDeviceSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.initRulesDialogSubscription();
    this.initOffersDialogSubscription();
    this.initReviewsDialogSubscription();
  }

  onLogin(form: NgForm) {
    if (!form.valid || this.isLoading) {
      return;
    }
    this.isLoading = true;
    this.loginSubscription = this.authService.login(form.value.phoneNumber, form.value.password).subscribe(
      response => {
        this.isLoading = false;
        if (this.data?.closeAfterLogin) {
          this.dialogRef.close();
        }
      },
      error => this.isLoading = false
    );
  }

  onLogout() {
    this.authService.logout();
  }
}
