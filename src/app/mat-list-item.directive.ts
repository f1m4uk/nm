import { Directive, ElementRef, HostListener } from '@angular/core';
import { AnimationBuilder, animate, style, group, query } from '@angular/animations';

@Directive({
  selector: 'mat-list-item'
})
export class MatListItemDirective {
  animationTime = 200;
  panStart: {
    listItem?: {
      clientRect: ClientRect
    },
    suffixActions?: {
      clientRect: ClientRect,
      offsetRight: number
    },
    prefixActions?: {
      clientRect: ClientRect,
      offsetLeft: number
    }
  } = {};

  constructor(
    private elementRef: ElementRef,
    private animationBuilder: AnimationBuilder
  ) { }

  @HostListener('panstart')
  onPanStart() {
    const nativeElement = this.elementRef.nativeElement as HTMLElement;
    for (let index = 0; index < nativeElement.parentElement.children.length; index++) {
      const element = nativeElement.parentElement.children[index] as HTMLElement;
      if (!element.isSameNode(nativeElement)) {
        this.resetElement(element);
      }
	}
	const suffixActions = nativeElement.querySelector<HTMLElement>('[appMatListSuffixActions]');
	const prefixActions = nativeElement.querySelector<HTMLElement>('[appMatListPrefixActions]');
    this.panStart = {
      listItem: {
        clientRect: nativeElement?.getBoundingClientRect()
      },
      suffixActions: {
        clientRect: suffixActions?.getBoundingClientRect(),
        offsetRight: window.innerWidth - (suffixActions?.offsetLeft + suffixActions?.offsetWidth)
      },
      prefixActions: {
        clientRect: prefixActions?.getBoundingClientRect(),
        offsetLeft: prefixActions?.offsetLeft
      }
    }
  }

  @HostListener('panleft', ['$event'])
  @HostListener('panright', ['$event'])
  onPanHorizontally(event) {
    const nativeElement: HTMLElement = this.elementRef.nativeElement;
    const prefixActionsWidth = this.panStart.prefixActions.clientRect?.width + event.deltaX;
    const suffixActionsWidth = this.panStart.suffixActions.clientRect?.width - event.deltaX;

	let listItemLeft = this.panStart.listItem.clientRect.left + event.deltaX;
	if (
		(listItemLeft > 0 && !this.panStart.prefixActions.clientRect) ||
		(listItemLeft < 0 && !this.panStart.suffixActions.clientRect)
	) {
		listItemLeft = 0;
	}

    this.animationBuilder.build(
      group([
        animate(0, style({ left: listItemLeft })),
        query('[appMatListPrefixActions]', [
          animate(0, style({
			width: prefixActionsWidth > 0 && listItemLeft !== 0 ? prefixActionsWidth : 0,
			left: listItemLeft !== 0 ? -Math.abs(this.panStart.prefixActions.offsetLeft - event.deltaX) : 0
		  }))
        ], { optional: true }),
        query('[appMatListSuffixActions]', [
          animate(0, style({
			width: suffixActionsWidth > 0 && listItemLeft !== 0 ? suffixActionsWidth : 0,
			right: listItemLeft !== 0 ? -Math.abs(this.panStart.suffixActions.offsetRight + event.deltaX) : 0
		  }))
        ], { optional: true })
      ])
    ).create(nativeElement).play();
  }

  @HostListener('panend')
  onPanEnd() {
    const nativeElement: HTMLElement = this.elementRef.nativeElement;
	const clientRect: ClientRect = nativeElement.getBoundingClientRect();

	if (clientRect.left === 0) return;

    const actionsAmount = nativeElement.querySelector(clientRect.left > 0 ? '[appMatListPrefixActions]' : '[appMatListSuffixActions]')?.querySelectorAll('button').length || 0;
    const actionsWidth = clientRect.height * actionsAmount;

    this.animationBuilder.build(
      group([
        animate(this.animationTime, style({ left: clientRect.left > 0 ? actionsWidth : -actionsWidth })),
        query('[appMatListPrefixActions]', [
          animate(this.animationTime, style({ width: actionsWidth, left: -actionsWidth}))
        ], { optional: true }),
        query('[appMatListSuffixActions]', [
          animate(this.animationTime, style({ width: actionsWidth, right: -actionsWidth }))
        ], { optional: true })
      ])
    ).create(nativeElement).play();

    document.addEventListener('click', () => { this.resetElement(nativeElement) }, {once: true});
  }

  resetElement(element: HTMLElement) {
    this.animationBuilder.build(
      group([
        animate(this.animationTime, style({ left: 0 })),
        query('[appMatListPrefixActions]', [
          animate(this.animationTime, style({ width: 0, left: 0}))
        ], { optional: true }),
        query('[appMatListSuffixActions]', [
          animate(this.animationTime, style({ width: 0, right: 0}))
        ], { optional: true })
      ])
    ).create(element).play();
  }
}
