import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay, switchMap } from 'rxjs/operators';
import { ServicesService } from 'src/app/services/services.service';
import { Service } from 'src/app/services/service.model';

@Component({
  selector: 'app-hotel-service',
  templateUrl: './hotel-service.component.html',
  styleUrls: ['./hotel-service.component.css']
})
export class HotelServiceComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  hotelService$: Observable<Service> = this.activatedRoute.paramMap.pipe(
    switchMap(paramMap => this.servicesService.getService(paramMap.get('id'))),
    shareReplay()
  );

  window: Window = window;
  constructor(
    private activatedRoute: ActivatedRoute,
    private breakpointObserver: BreakpointObserver,
    private servicesService: ServicesService
  ) { }

  ngOnInit(): void {}

}
