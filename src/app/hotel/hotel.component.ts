import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { ServicesService } from '../services/services.service';
import { Service } from '../services/service.model';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.css']
})
export class HotelComponent implements OnInit, OnDestroy {
  isBackdropCollapsed = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  hotelServices$: Observable<Service[]> = this.servicesService.getHotelServices().pipe(shareReplay());

  hotelServicesSubscription: Subscription = this.hotelServices$.subscribe(hotelServices => this.selectedHotelService$ = this.servicesService.getService(hotelServices[0]?._id).pipe(shareReplay()));

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  selectedHotelService$: Observable<Service>;

  window: Window = window;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private servicesService: ServicesService,
    private translocoService: TranslocoService
  ) { }

  ngOnDestroy(): void {
    this.hotelServicesSubscription.unsubscribe();
  }

  ngOnInit(): void {
  }


  onHotelServiceSelect(hotelService: Service) {
    this.selectedHotelService$ = this.servicesService.getService(hotelService._id).pipe(shareReplay());
    this.isBackdropCollapsed = false;
  }
}
