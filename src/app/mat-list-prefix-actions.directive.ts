import { Directive } from '@angular/core';

@Directive({
  selector: '[appMatListPrefixActions]',
  host: {
    '[style.height]': '"100%"',
    '[style.position]': "'absolute'",
    '[style.left]': '0',
    '[style.display]': "'flex'",
    '[style.flex-direction]': "'row'",
    '[style.box-sizing]': "'border-box'",
    '[style.width]': '0'
  }
})
export class MatListPrefixActionsDirective {

  constructor() { }

}
