import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopFloatingToolbarComponent } from './top-floating-toolbar.component';

describe('TopFloatingToolbarComponent', () => {
  let component: TopFloatingToolbarComponent;
  let fixture: ComponentFixture<TopFloatingToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopFloatingToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopFloatingToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
