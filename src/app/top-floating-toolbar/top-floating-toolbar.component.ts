import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay, filter } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ProfileDialogComponent } from '../profile-dialog/profile-dialog.component';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-top-floating-toolbar',
  templateUrl: './top-floating-toolbar.component.html',
  styleUrls: ['./top-floating-toolbar.component.css']
})
export class TopFloatingToolbarComponent implements OnInit {
  isHidden$ = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    map((event: NavigationEnd) => event.url === '/'),
    shareReplay()
  );
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private matDialog: MatDialog,
    private router: Router
    ) { }

  ngOnInit(): void {}

  onProfileButtonClick(): void {
    this.matDialog.open(ProfileDialogComponent, {
      panelClass: 'relatively-positioned-dialog'
    });
  }

}
