import { RoomType } from '../room-types/room-type.model';

export interface Room {
  _id: string,
  name_en?: string,
  name_ru: string,
  name_uk?: string,
  type: RoomType
}
