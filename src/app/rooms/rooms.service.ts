import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Room } from './room.model';
import { environment } from 'src/environments/environment';

const ROOMS_URL = environment.API_URL + '/rooms';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private httpClient: HttpClient) { }

  createRoom(body: any): Observable<{message: string, result: Room}> {
    return this.httpClient.post<{message: string, result: Room}>(ROOMS_URL, body);
  }

  deleteRoom(id: string): Observable<Room> {
    return this.httpClient.delete<Room>(ROOMS_URL + '/' + id);
  }

  getRooms(): Observable<Room[]> {
    return this.httpClient.get<Room[]>(ROOMS_URL);
  }

  getRoom(id: string): Observable<Room> {
    return this.httpClient.get<Room>(ROOMS_URL + '/' + id);
  }

  updateRoom(id: string, body): Observable<{message: string, result: Room}> {
    return this.httpClient.put<{message: string, result: Room}>(ROOMS_URL + '/' + id, body);
  }
}
