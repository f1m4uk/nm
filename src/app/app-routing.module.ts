import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HotelComponent } from './hotel/hotel.component';
import { AdditionalServicesComponent } from './additional-services/additional-services.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth/auth.guard';
import { RoleGuard } from './role/role.guard';
import { RoomTypesComponent } from './room-types/room-types.component';


const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'room-types', component: RoomTypesComponent },
  { path: 'hotel', component: HotelComponent },
  { path: 'additional-services', component: AdditionalServicesComponent },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard, RoleGuard],
    data: {expectedRole: 'admin'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
