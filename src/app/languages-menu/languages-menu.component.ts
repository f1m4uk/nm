import { Component, OnInit, ViewChild } from '@angular/core';
import { AvailableLangs, TranslocoService } from '@ngneat/transloco';
import { MatMenu } from '@angular/material/menu';

@Component({
  selector: 'app-languages-menu',
  templateUrl: './languages-menu.component.html',
  styleUrls: ['./languages-menu.component.css']
})
export class LanguagesMenuComponent implements OnInit {
  availableLanguages: AvailableLangs = this.translocoService.getAvailableLangs();
  @ViewChild(MatMenu, { static: true }) menu: MatMenu;

  constructor(private translocoService: TranslocoService) { }

  ngOnInit(): void {
    const savedLanguage = localStorage.getItem('language');
    if (savedLanguage) {
      this.translocoService.setActiveLang(savedLanguage);
    }
  }

  onLanguageChange(languageId: string): void {
    this.translocoService.setActiveLang(languageId);
    localStorage.setItem('language', languageId);
  }
}
