import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsRailComponent } from './actions-rail.component';

describe('ActionsRailComponent', () => {
  let component: ActionsRailComponent;
  let fixture: ComponentFixture<ActionsRailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsRailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsRailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
