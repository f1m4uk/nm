import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Observable, combineLatest, Subject, Subscription } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay, filter, tap } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProfileDialogComponent } from '../profile-dialog/profile-dialog.component';
import { NavigationEnd, Router } from '@angular/router';
import { RulesDialogComponent } from '../rules/rules-dialog/rules-dialog.component';
import { OffersDialogComponent } from '../offers/offers-dialog/offers-dialog.component';
import { ReviewsDialogComponent } from '../reviews/reviews-dialog/reviews-dialog.component';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-actions-rail',
  templateUrl: './actions-rail.component.html',
  styleUrls: ['./actions-rail.component.css']
})
export class ActionsRailComponent implements OnInit, OnDestroy {
  isLoggedIn$ = this.authService.isLoggedIn$.pipe(shareReplay());
  isHidden$ = this.router.events.pipe(
    filter(event => event instanceof NavigationEnd),
    map((event: NavigationEnd) => event.url === '/'),
    shareReplay()
  );
  isTablet$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Tablet)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  @ViewChild('offersAction', { read: ElementRef }) offersAction: ElementRef;

  offersDialog: MatDialogRef<OffersDialogComponent>;
  offersDialogOnDeviceSubscription: Subscription;
  offersDialogSubject = new Subject();

  @ViewChild('profileAction', { read: ElementRef }) profileAction: ElementRef;

  @ViewChild('reviewsAction', { read: ElementRef }) reviewsAction: ElementRef;

  reviewsDialog: MatDialogRef<ReviewsDialogComponent>;
  reviewsDialogOnDeviceSubscription: Subscription;
  reviewsDialogSubject = new Subject();

  @ViewChild('rulesAction', { read: ElementRef }) rulesAction: ElementRef;

  rulesDialog: MatDialogRef<RulesDialogComponent>;
  rulesDialogOnDeviceSubscription: Subscription;
  rulesDialogSubject = new Subject();

  constructor(
    private authService: AuthService,
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    private matDialog: MatDialog
  ) { }

  initOffersDialogSubscription() {
    this.offersDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.offersDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const offersDialogSubjectValue = result[1];
      if (!offersDialogSubjectValue) {
        return;
      }
      const offersActionHTMLElement = this.offersAction?.nativeElement as HTMLElement;
      const rect = offersActionHTMLElement.getBoundingClientRect();
      const dialogWidth = 560;
      if (!this.offersDialog?.componentInstance) {
        this.offersDialog = this.matDialog.open(OffersDialogComponent, {
          panelClass: 'full-screen-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          position: {
            top: rect.top + 'px',
            right: rect.right + dialogWidth + 'px',
            bottom: rect.bottom + 'px',
            left: rect.left - dialogWidth + 'px'
          },
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.offersDialog.updateSize('100%', '100%');
      } else {
        this.offersDialog.updateSize(dialogWidth + 'px');
      }
      this.offersDialog.afterClosed().toPromise().then(result => this.offersDialogSubject.next(false));
    });
  }

  initReviewsDialogSubscription() {
    this.reviewsDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.reviewsDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const reviewsDialogSubjectValue = result[1];
      if (!reviewsDialogSubjectValue) {
        return;
      }
      const reviewsActionHTMLElement = this.reviewsAction?.nativeElement as HTMLElement;
      const rect = reviewsActionHTMLElement.getBoundingClientRect();
      const dialogWidth = 560;
      if (!this.reviewsDialog?.componentInstance) {
        this.reviewsDialog = this.matDialog.open(ReviewsDialogComponent, {
          panelClass: ['full-screen-dialog', 'relatively-positioned-dialog', 'no-scroll-x'],
          maxWidth: '100vw',
          maxHeight: '100vh',
          position: {
            top: rect.top + 'px',
            right: rect.right + dialogWidth + 'px',
            bottom: rect.bottom + 'px',
            left: rect.left - dialogWidth + 'px'
          },
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.reviewsDialog.updateSize('100%', '100%');
      } else {
        this.reviewsDialog.updateSize(dialogWidth + 'px');
      }
      this.reviewsDialog.afterClosed().toPromise().then(result => this.reviewsDialogSubject.next(false));
    });
  }

  initRulesDialogSubscription() {
    this.rulesDialogOnDeviceSubscription = combineLatest(this.isHandset$, this.rulesDialogSubject).subscribe(result => {
      const isHandset = result[0];
      const rulesDialogSubjectValue = result[1];
      if (!rulesDialogSubjectValue) {
        return;
      }
      const rulesActionHTMLElement = this.rulesAction?.nativeElement as HTMLElement;
      const rect = rulesActionHTMLElement.getBoundingClientRect();
      const dialogWidth = 560;
      if (!this.rulesDialog?.componentInstance) {
        this.rulesDialog = this.matDialog.open(RulesDialogComponent, {
          panelClass: 'full-screen-dialog',
          maxWidth: '100vw',
          maxHeight: '100vh',
          position: {
            top: rect.top + 'px',
            right: rect.right + dialogWidth + 'px',
            bottom: rect.bottom + 'px',
            left: rect.left - dialogWidth + 'px'
          },
          width: dialogWidth + 'px'
        });
      }
      if (isHandset) {
        this.rulesDialog.updateSize('100%', '100%');
      } else {
        this.rulesDialog.updateSize(dialogWidth + 'px');
      }
      this.rulesDialog.afterClosed().toPromise().then(result => this.rulesDialogSubject.next(false));
    });
  }

  ngOnDestroy(): void {
    this.rulesDialogOnDeviceSubscription?.unsubscribe();
    this.offersDialogOnDeviceSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    this.initRulesDialogSubscription();
    this.initOffersDialogSubscription();
    this.initReviewsDialogSubscription();
  }

  onProfileActionClick(): void {
    const profileActionHTMLElement = this.profileAction.nativeElement as HTMLElement;
    const rect = profileActionHTMLElement.getBoundingClientRect();
    const dialogWidth = 560;
    this.matDialog.open(ProfileDialogComponent, {
      position: {
        top: rect.top + 'px',
        right: rect.right + dialogWidth + 'px',
        bottom: rect.bottom + 'px',
        left: rect.left - dialogWidth + 'px'
      },
      width: dialogWidth + 'px',
      panelClass: 'relatively-positioned-dialog'
    });
  }
}
