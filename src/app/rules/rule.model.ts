export interface Rule {
  _id: string,
  description_en?: string,
  description_ru: string,
  description_uk?: string
}
