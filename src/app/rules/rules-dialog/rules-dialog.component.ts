import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';
import { RulesService } from '../rules.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { TranslocoService } from '@ngneat/transloco';
import { Rule } from '../rule.model';

@Component({
  selector: 'app-rules-dialog',
  templateUrl: './rules-dialog.component.html',
  styleUrls: ['./rules-dialog.component.css']
})
export class RulesDialogComponent implements OnInit {
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  rules$: Observable<Rule[]> = this.rulesService.getRules().pipe(shareReplay());

  window: Window = window;

  constructor(
    private rulesService: RulesService,
    private breakpointObserver: BreakpointObserver,
    private translocoService: TranslocoService
  ) { }

  ngOnInit(): void {}
}
