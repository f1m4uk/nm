import { Injectable } from '@angular/core';
import { Rule } from './rule.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const RULES_URL = environment.API_URL + '/rules';

@Injectable({
  providedIn: 'root'
})
export class RulesService {

  constructor(private httpClient: HttpClient) { }

  createRule(body: any): Observable<{message: string, result: Rule}> {
    return this.httpClient.post<{message: string, result: Rule}>(RULES_URL, body);
  }

  deleteRule(id: string): Observable<Rule> {
    return this.httpClient.delete<Rule>(RULES_URL + '/' + id);
  }

  getRules(): Observable<Rule[]> {
    return this.httpClient.get<Rule[]>(RULES_URL);
  }

  getRule(id: string): Observable<Rule> {
    return this.httpClient.get<Rule>(RULES_URL + '/' + id);
  }

  updateRule(id: string, body): Observable<{message: string, result: Rule}> {
    return this.httpClient.put<{message: string, result: Rule}>(RULES_URL + '/' + id, body);
  }
}
