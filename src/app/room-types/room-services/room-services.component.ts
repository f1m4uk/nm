import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Service } from 'src/app/services/service.model';
import { shareReplay } from 'rxjs/operators';
import { ServicesService } from 'src/app/services/services.service';
import { TranslocoService } from '@ngneat/transloco';
import { RoomType } from '../room-type.model';

@Component({
  selector: 'app-room-services',
  templateUrl: './room-services.component.html',
  styleUrls: ['./room-services.component.css']
})
export class RoomServicesComponent implements OnInit {
  @Input() roomType: RoomType;

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());
  roomServices$: Observable<Service[]> = this.servicesService.getRoomsServices().pipe(shareReplay());

  window: Window = window;

  constructor(
    private servicesService: ServicesService,
    private translocoService: TranslocoService
  ) { }

  /**
   * True when requested services instance is presented in the room services list.
   * @param service Service to search in the room.
   */
  isRoomServiceAvailable(service: Service) {
    return !!this.roomType.services.find(roomService => roomService._id === service._id);
  }

  ngOnInit(): void {
  }

}
