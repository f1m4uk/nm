import { Injectable } from '@angular/core';
import { RoomType } from './room-type.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const ROOM_TYPES_URL = environment.API_URL + '/room-types';

@Injectable({
  providedIn: 'root'
})
export class RoomTypesService {

  constructor(private httpClient: HttpClient) { }

  createRoomType(body: any): Observable<{message: string, result: RoomType}> {
    const formData = new FormData();
    for (let field in body) {
      const value = body[field];
      if (Array.isArray(value)) {
        for (let valueField of value) {
          if (valueField !== null) {
            formData.append(field, valueField);
          }
        }
      } else {
        if (value !== null) {
          formData.append(field, value);
        }
      }
    }
    return this.httpClient.post<{message: string, result: RoomType}>(ROOM_TYPES_URL, formData);
  }

  deleteRoomType(id: string): Observable<RoomType> {
    return this.httpClient.delete<RoomType>(ROOM_TYPES_URL + '/' + id);
  }

  getRoomTypes(): Observable<RoomType[]> {
    return this.httpClient.get<RoomType[]>(ROOM_TYPES_URL);
  }

  getRoomType(id: string): Observable<RoomType> {
    return this.httpClient.get<RoomType>(ROOM_TYPES_URL + '/' + id);
  }

  updateRoomType(id: string, body): Observable<{message: string, result: RoomType}> {
    const formData = new FormData();
    for (let field in body) {
      const value = body[field];
      if (Array.isArray(value)) {
        for (let valueField of value) {
          formData.append(field, valueField);
        }
      } else {
        formData.append(field, value);
      }
    }
    return this.httpClient.put<{message: string, result: RoomType}>(ROOM_TYPES_URL + '/' + id, formData);
  }
}
