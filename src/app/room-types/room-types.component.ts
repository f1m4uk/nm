import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { shareReplay, map } from 'rxjs/operators';
import { RoomType } from './room-type.model';
import { RoomTypesService } from './room-types.service';
import { TranslocoService } from '@ngneat/transloco';

@Component({
  selector: 'app-room-types',
  templateUrl: './room-types.component.html',
  styleUrls: ['./room-types.component.css']
})
export class RoomTypesComponent implements OnInit, OnDestroy {
  isBackdropCollapsed = true;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  isTablet$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Tablet)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  language$: Observable<string> = this.translocoService.langChanges$.pipe(shareReplay());

  roomTypes$: Observable<RoomType[]> = this.roomTypesService.getRoomTypes().pipe(shareReplay());

  roomTypesSubscription: Subscription = this.roomTypes$.subscribe(roomTypes => this.selectedRoomType$ = this.roomTypesService.getRoomType(roomTypes[0]?._id).pipe(shareReplay()));

  selectedRoomType$: Observable<RoomType>;

  window: Window = window;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private roomTypesService: RoomTypesService,
    private translocoService: TranslocoService
  ) { }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.roomTypesSubscription.unsubscribe();
  }

  onRoomTypeSelect(roomType: RoomType) {
    this.selectedRoomType$ = this.roomTypesService.getRoomType(roomType._id).pipe(shareReplay());
    this.isBackdropCollapsed = false;
  }
}
