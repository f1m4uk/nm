import { Service } from '../services/service.model';

export interface RoomType {
  services?: Service[],
  imagePaths?: string[],
  _id: string,
  name_en?: string,
  name_ru: string,
  name_uk?: string,
  description_en?: string
  description_ru: string
  description_uk?: string
}
