import { Component, OnInit, Input, OnDestroy, NgZone, ViewChild, AfterViewChecked, ChangeDetectorRef, EventEmitter, Output } from '@angular/core';
import { CdkScrollable, ScrollDispatcher } from '@angular/cdk/overlay';
import { Subscription, Observable } from 'rxjs';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild('carousel') carousel;
  @Output() imagePathsChange = new EventEmitter<string[]>();
  @Output() imagePathRemoved = new EventEmitter<string>();
  @Input() imagePaths: string[];

  isCarouselScrollable = false;
  @Input() isImageRemovable = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );
  isScrolledFromLeft = false;
  isScrolledFromRight = true;

  scrollSubscription: Subscription;

  window: Window = window;

  constructor(
    private scrollDispatcher: ScrollDispatcher,
    private ngZone: NgZone,
    private changeDetectorRef: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver
  ) { }

  ngAfterViewChecked(): void {
    if (this.carousel) {
      this.isCarouselScrollable = this.carousel.nativeElement.clientWidth < this.carousel.nativeElement.scrollWidth;
      this.changeDetectorRef.detectChanges();
    }
  }

  ngOnInit(): void {
    this.scrollSubscription = this.scrollDispatcher.scrolled().subscribe(cdkScrollable => {
      if (cdkScrollable instanceof CdkScrollable) {
        this.ngZone.run(() => {
          this.isScrolledFromLeft = cdkScrollable.measureScrollOffset('left') > 0;
          this.isScrolledFromRight = cdkScrollable.measureScrollOffset('right') > 0;
        });
      }
    })
  }

  ngOnDestroy(): void {
    this.scrollSubscription.unsubscribe();
  }

  onImageRemove(index: number) {
    this.imagePathRemoved.emit(this.imagePaths.splice(index, 1)[0]);
    this.imagePathsChange.emit(this.imagePaths);
  }

  scrollToLeft(): void {
    const carouselHTMLElement = this.carousel.nativeElement as HTMLElement;
    carouselHTMLElement.scrollTo({left: carouselHTMLElement.scrollLeft - 344, behavior: 'smooth'})
  }

  scrollToRight(): void {
    const carouselHTMLElement = this.carousel.nativeElement as HTMLElement;
    carouselHTMLElement.scrollTo({left: carouselHTMLElement.scrollLeft + 344, behavior: 'smooth'})
  }

}
