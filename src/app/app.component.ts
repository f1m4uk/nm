import { Component, OnInit, OnDestroy } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslocoService } from '@ngneat/transloco';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  snackBarSubscription: Subscription;
  swUpdateSubscription: Subscription;

  constructor(
    private swUpdate: SwUpdate,
    private matSnackBar: MatSnackBar,
    private translocoService: TranslocoService
  ) {}

  ngOnDestroy(): void {
    this.snackBarSubscription?.unsubscribe();
    this.swUpdateSubscription?.unsubscribe();
  }

  ngOnInit(): void {
    if (this.swUpdate.isEnabled) {
     this.swUpdateSubscription = this.swUpdate.available.subscribe(() => {
        this.snackBarSubscription = this.matSnackBar.open(
          this.translocoService.translate('update-available.message'),
          this.translocoService.translate('update-available.action')
        ).onAction().subscribe(() => window.location.reload());
      });
    }
  }
}
