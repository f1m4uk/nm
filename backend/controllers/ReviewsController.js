import { Review } from "../models/review";
import jwt from 'jsonwebtoken';

export class ReviewsController {
  static async createReview(request, response) {
    const requestBody = request.body;
    const decodedToken = jwt.decode(request.verifiedJsonWebToken);
    if ('isApproved' in requestBody || 'adminNotes' in requestBody) {
      if (decodedToken.role !== 'admin') {
        delete requestBody.isApproved;
        delete requestBody.adminNotes;
      }
    }
    if (request.file) {
      const url = request.protocol + '://' + request.get('host');
      requestBody.imagePath = url + '/images/' + request.file.filename;
    }
    const review = new Review({...requestBody, ...{
      user: decodedToken.userId,
      date: new Date()
    }});
    try {
      const savedReview = await review.save();
      response.status(201).json({
        message: 'Review created successfully!',
        result: savedReview
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readReview(request, response) {
    try {
     const foundReview = await Review.findById(request.params.id);
     if (foundReview) {
       response.status(200).json(foundReview);
     } else {
       response.status(404).json({
         message: 'Review wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readReviews(request, response) {
    try {
      const queryParams = {
        isApproved: true
      };
      let decodedToken;
      try {
        const token = request.headers.authorization.split(' ')[1];
        jwt.verify(token, process.env.JWT_SECRET);
        decodedToken = jwt.decode(token);
        if (decodedToken.role === 'admin') {
          delete queryParams.isApproved;
        }
      } catch(error) {
        // ignore
      }
      const foundReviews = await Review.find(queryParams).sort('-date').populate('user', '_id');
      if (decodedToken) {
        const userReview = await Review.findOne({user: decodedToken.userId}).populate('user', '_id');
        if (userReview && decodedToken.role !== 'admin' && !userReview.isApproved) {
          foundReviews.unshift(userReview);
        }
      }

      response.status(200).json(foundReviews);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateReview(request, response) {
    const requestBody = request.body;

    if (request.file) {
      const url = request.protocol + '://' + request.get('host');
      requestBody.imagePath = url + '/images/' + request.file.filename;
    }

    try {
      const foundAndUpdatedReview = await Review.findByIdAndUpdate(request.params.id, requestBody, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedReview) {
        response.status(200).json({
          message: 'Review updated successfully!',
          result: foundAndUpdatedReview
        });
      } else {
        response.status(404).json({
          message: 'Review wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteReview(request, response) {
    try {
      const foundAndDeletedReview = await Review.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedReview) {
        response.status(200).json({
          message: 'Review deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Review wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
