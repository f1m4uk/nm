import { RoomType } from "../models/roomType";

export class RoomTypesController {
  static async createRoomType(request, response) {
    const requestBody = request.body;
    const url = request.protocol + '://' + request.get('host');
    const roomType = new RoomType({...requestBody, ...{
      services: requestBody.servicesIds,
      imagePaths: request.files.map(file => url + '/images/' + file.filename)
    }});
    try {
      const savedRoomType = await roomType.save();
      response.status(201).json({
        message: 'Room type created successfully!',
        result: savedRoomType
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRoomType(request, response) {
    try {
     const foundRoomType = await RoomType.findById(request.params.id).populate('services');
     if (foundRoomType) {
       response.status(200).json(foundRoomType);
     } else {
       response.status(404).json({
         message: 'Room type wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRoomTypes(request, response) {
    try {
      const foundRoomTypes = await RoomType.find().populate('services');
      response.status(200).json(foundRoomTypes);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateRoomType(request, response) {
    const requestBody = request.body;

    requestBody.imagePaths = [].concat(requestBody.imagePaths || []);

    if (request.files) {
      const url = request.protocol + '://' + request.get('host');
      requestBody.imagePaths = [...request.files.map(file => url + '/images/' + file.filename), ...requestBody.imagePaths];
    }

    requestBody.services = requestBody.servicesIds || [];
    delete requestBody.servicesIds;

    try {
      const foundAndUpdatedRoomType = await RoomType.findByIdAndUpdate(request.params.id, requestBody, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedRoomType) {
        response.status(200).json({
          message: 'Room type updated successfully!',
          result: foundAndUpdatedRoomType
        });
      } else {
        response.status(404).json({
          message: 'Room type wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteRoomType(request, response) {
    try {
      const foundAndDeletedRoomType = await RoomType.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedRoomType) {
        response.status(200).json({
          message: 'Room type deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Room type wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static remap
}
