import { Guest } from "../models/guest";

export class GuestsController {
  static async createGuest(request, response) {
    const requestBody = request.body;
    const guest = new Guest({
      ...requestBody,
      ...{
        room: requestBody.roomId,
        phoneNumber: process.env.PHONE_NUMBER_COUNTRY_EXTENSION + requestBody.phoneNumber
      }
    });
    try {
      const savedGuest = await guest.save();
      response.status(201).json({
        message: 'Guest created successfully!',
        result: savedGuest
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readGuest(request, response) {
    try {
     const foundGuest = await Guest.findById(request.params.id).populate('room');
     if (foundGuest) {
       response.status(200).json(foundGuest);
     } else {
       response.status(404).json({
         message: 'Guest wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readGuests(request, response) {
    try {
      const foundGuests = await Guest.find().populate('room');
      response.status(200).json(foundGuests);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateGuest(request, response) {
    const requestBody = request.body;
    try {
      const foundAndUpdatedGuest = await Guest.findByIdAndUpdate(request.params.id, {
        ...requestBody,
        ...{
          room: requestBody.roomId,
          phoneNumber: process.env.PHONE_NUMBER_COUNTRY_EXTENSION + requestBody.phoneNumber
        }
      }, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedGuest) {
        response.status(200).json({
          message: 'Guest updated successfully!',
          result: foundAndUpdatedGuest
        });
      } else {
        response.status(404).json({
          message: 'Guest wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteGuest(request, response) {
    try {
      const foundAndDeletedGuest = await Guest.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedGuest) {
        response.status(200).json({
          message: 'Guest deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Guest wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
