import { Offer } from "../models/offer";

export class OffersController {
  static async createOffer(request, response) {
    const offer = new Offer(request.body);
    try {
      const savedOffer = await offer.save();
      response.status(201).json({
        message: 'Offer created successfully!',
        result: savedOffer
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readOffer(request, response) {
    try {
     const foundOffer = await Offer.findById(request.params.id);
     if (foundOffer) {
       response.status(200).json(foundOffer);
     } else {
       response.status(404).json({
         message: 'Offer wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readOffers(request, response) {
    try {
      const foundOffers = await Offer.find();
      response.status(200).json(foundOffers);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateOffer(request, response) {
    const requestBody = request.body;
    try {
      const foundAndUpdatedOffer = await Offer.findByIdAndUpdate(request.params.id, requestBody, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedOffer) {
        response.status(200).json({
          message: 'Offer updated successfully!',
          result: foundAndUpdatedOffer
        });
      } else {
        response.status(404).json({
          message: 'Offer wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteOffer(request, response) {
    try {
      const foundAndDeletedOffer = await Offer.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedOffer) {
        response.status(200).json({
          message: 'Offer deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Offer wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
