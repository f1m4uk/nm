import { Service } from "../models/service";

export class ServicesController {
  static async createService(request, response) {
    const requestBody = request.body;
    if (request.files) {
      const url = request.protocol + '://' + request.get('host');
      requestBody.imagePaths = request.files.map(file => url + '/images/' + file.filename);
    }
    const service = new Service(requestBody);
    try {
      const savedService = await service.save();
      response.status(201).json({
        message: 'Service created successfully!',
        result: savedService
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readService(request, response) {
    try {
     const foundService = await Service.findById(request.params.id);
     if (foundService) {
       response.status(200).json(foundService);
     } else {
       response.status(404).json({
         message: 'Service wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readServices(request, response) {
    try {
      const foundServices = await Service.find(request.query);
      response.status(200).json(foundServices);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateService(request, response) {
    const requestBody = request.body;

    requestBody.imagePaths = [].concat(requestBody.imagePaths || []);

    if (request.files) {
      const url = request.protocol + '://' + request.get('host');
      requestBody.imagePaths = [...request.files.map(file => url + '/images/' + file.filename), ...requestBody.imagePaths];
    }

    try {
      const foundAndUpdatedService = await Service.findByIdAndUpdate(request.params.id, requestBody, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedService) {
        response.status(200).json({
          message: 'Service updated successfully!',
          result: foundAndUpdatedService
        });
      } else {
        response.status(404).json({
          message: 'Service wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteService(request, response) {
    try {
      const foundAndDeletedService = await Service.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedService) {
        response.status(200).json({
          message: 'Service deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Service wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
