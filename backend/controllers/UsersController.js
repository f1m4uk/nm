import { Guest } from "../models/guest";
import { User } from "../models/user";

import bcrypt from "bcrypt";
import jwt from 'jsonwebtoken';

export class UsersController {
  static async createUser(request, response) {
    try {
      const appropriateGuest = await Guest.findOne({
        phoneNumber: process.env.PHONE_NUMBER_COUNTRY_EXTENSION + request.body.phoneNumber
      });
      if (!appropriateGuest) {
        return response.status(404).json({
          message: 'Appropriate guest wasn\'t found!'
        });
      }
      const now = new Date();
      if (now < appropriateGuest.startDate || now > appropriateGuest.endDate ) {
        return response.status(403).json({
          message: 'Appropriate guest is not allowed to be user at the moment!'
        })
      }

      const user = new User({
        phoneNumber: appropriateGuest.phoneNumber
      });
      const savedUser = await user.save();
      return savedUser;
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async loginUser(request, response) {
    try {
      const foundUser = await User.findOne({
        phoneNumber: process.env.PHONE_NUMBER_COUNTRY_EXTENSION + request.body.phoneNumber
      }) || await UsersController.createUser(request, response);

      if (!(foundUser instanceof User)) {
        return;
      }

      if (!foundUser) {
        return response.status(404).json({
          message: 'User wasn\'t found!'
        });
      }

      // if has password
      if (request.body.password && foundUser.password) {
        const isPasswordCorrect = await bcrypt.compare(request.body.password, foundUser.password);
        if (!isPasswordCorrect) {
          return response.status(401).json({
            message: 'Authentication failed!'
          });
        }
        const token = jwt.sign({
          phoneNumber: foundUser.phoneNumber,
          userId: foundUser._id,
          role: 'admin'
        }, process.env.JWT_SECRET, {
          expiresIn: '1d'
        });

        return response.status(200).json({
          token: token
        });
      }

      // if has no password - check if the user is current guest
      const appropriateGuests = await Guest.find({
        phoneNumber: foundUser.phoneNumber
      });
      if (appropriateGuests.length === 0) {
        return response.status(404).json({
          message: 'Appropriate guests weren\'t found!'
        });
      }

      const now = new Date();

      const currentGuest = appropriateGuests.find(appropriateGuest => appropriateGuest.startDate < now && now < appropriateGuest.endDate);

      if (!currentGuest) {
        return response.status(403).json({
          message: 'The user is not allowed to log in at the moment!'
        });
      }

      const token = jwt.sign({
        phoneNumber: foundUser.phoneNumber,
        userId: foundUser._id
      }, process.env.JWT_SECRET, {
        expiresIn: '1d'
      });

      return response.status(200).json({
        token: token
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readCurrentUser(request, response) {
    try {
      const verifiedJsonWebToken = request.verifiedJsonWebToken;
      const decodedToken = jwt.decode(verifiedJsonWebToken);
      const foundUser = await User.findById(decodedToken.userId).select('-password');

      if (!foundUser) {
        return response.status(404).json({
          message: 'User wasn\'t found!'
        });
      }

      response.status(200).json(foundUser);
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readGuestsOfCurrentUser(request, response) {
    try {
      const verifiedJsonWebToken = request.verifiedJsonWebToken;
      const decodedToken = jwt.decode(verifiedJsonWebToken);
      const foundGuests = await Guest.find({phoneNumber: decodedToken.phoneNumber}).select('startDate endDate peopleAmount');
      response.status(200).json(foundGuests);
     } catch(error) {
      response.status(500).json(error);
     }
  }

  static async refreshSession(request, response) {
    try {
      const verifiedJsonWebToken = request.verifiedJsonWebToken;
      const decodedToken = jwt.decode(verifiedJsonWebToken);

      if (decodedToken.role !== 'admin') {
        const appropriateGuests = await Guest.find({
          phoneNumber: decodedToken.phoneNumber
        });
        if (appropriateGuests.length === 0) {
          return response.status(404).json({
            message: 'Appropriate guests weren\'t found!'
          });
        }

        const now = new Date();
        const currentGuest = appropriateGuests.find(appropriateGuest => appropriateGuest.startDate < now && now < appropriateGuest.endDate);
        if (!currentGuest) {
          return response.status(403).json({
            message: 'The user is not allowed to log in at the moment!'
          });
        }
      }

      delete decodedToken.iat;
      delete decodedToken.exp;

      response.status(200).json({
        token: jwt.sign(decodedToken, process.env.JWT_SECRET, {expiresIn: '1d'})
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
