import { Room } from "../models/room";

export class RoomsController {
  static async createRoom(request, response) {
    const requestBody = request.body;
    console.log(requestBody);
    const room = new Room({
      ...requestBody,
      ...{type: requestBody.typeId}
    });
    try {
      const savedRoom = await room.save();
      response.status(201).json({
        message: 'Room created successfully!',
        result: savedRoom
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRoom(request, response) {
    try {
     const foundRoom = await Room.findById(request.params.id).populate('type');
     if (foundRoom) {
       response.status(200).json(foundRoom);
     } else {
       response.status(404).json({
         message: 'Room wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRooms(request, response) {
    try {
      const foundRooms = await Room.find().populate('type');
      response.status(200).json(foundRooms);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateRoom(request, response) {
    const requestBody = request.body;
    try {
      const foundAndUpdatedRoom = await Room.findByIdAndUpdate(request.params.id, {
        ...requestBody,
        ...{type: requestBody.typeId}
      }, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedRoom) {
        response.status(200).json({
          message: 'Room updated successfully!',
          result: foundAndUpdatedRoom
        });
      } else {
        response.status(404).json({
          message: 'Room wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteRoom(request, response) {
    try {
      const foundAndDeletedRoom = await Room.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedRoom) {
        response.status(200).json({
          message: 'Room deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Room wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
