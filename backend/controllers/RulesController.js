import { Rule } from "../models/rule";

export class RulesController {
  static async createRule(request, response) {
    const rule = new Rule(request.body);
    try {
      const savedRule = await rule.save();
      response.status(201).json({
        message: 'Rule created successfully!',
        result: savedRule
      });
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRule(request, response) {
    try {
     const foundRule = await Rule.findById(request.params.id);
     if (foundRule) {
       response.status(200).json(foundRule);
     } else {
       response.status(404).json({
         message: 'Rule wasn\'t found!'
       });
     }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async readRules(request, response) {
    try {
      const foundRules = await Rule.find();
      response.status(200).json(foundRules);
     } catch(error) {
       response.status(500).json(error);
     }
  }

  static async updateRule(request, response) {
    const requestBody = request.body;
    try {
      const foundAndUpdatedRule = await Rule.findByIdAndUpdate(request.params.id, requestBody, {
        new: true,
        useFindAndModify: false
      });
      if (foundAndUpdatedRule) {
        response.status(200).json({
          message: 'Rule updated successfully!',
          result: foundAndUpdatedRule
        });
      } else {
        response.status(404).json({
          message: 'Rule wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }

  static async deleteRule(request, response) {
    try {
      const foundAndDeletedRule = await Rule.findByIdAndDelete(request.params.id, {
        useFindAndModify: false
      });
      if (foundAndDeletedRule) {
        response.status(200).json({
          message: 'Rule deleted successfully!'
        });
      } else {
        response.status(404).json({
          message: 'Rule wasn\'t found!'
        });
      }
    } catch(error) {
      response.status(500).json(error);
    }
  }
}
