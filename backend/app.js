import express from "express";
import mongoose from "mongoose";
import bodyParser from "body-parser";
import path from 'path';

import { servicesRouter } from "./routes/services";
import { roomTypesRouter } from "./routes/roomTypes";
import { roomsRouter } from "./routes/rooms";
import { guestsRouter } from "./routes/guests";
import { usersRouter } from "./routes/users";
import { reviewsRouter } from "./routes/reviews";
import { rulesRouter } from "./routes/rules";
import { offersRouter } from "./routes/offers";

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/images', express.static(path.join('images')));

mongoose.connect(`mongodb+srv://nick:${process.env.MONGO_ATLAS_PASSWORD}@cluster0-xvocz.mongodb.net/test?retryWrites=true&w=majority`, {
  useCreateIndex: true,
  useUnifiedTopology: true
})
  .then(() => {
    console.log('Connected to the database successfully!');
  })
  .catch(error => {
    console.error(`
      ERROR: Connection failed!
      ${error}
    `);
  });

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, PATCH, PUT, DELETE, OPTIONS'
    );
    next();
});

app.use('/api/services', servicesRouter);
app.use('/api/room-types', roomTypesRouter);
app.use('/api/rooms', roomsRouter);
app.use('/api/guests', guestsRouter);
app.use('/api/users', usersRouter);
app.use('/api/reviews', reviewsRouter);
app.use('/api/rules', rulesRouter);
app.use('/api/offers', offersRouter);

export { app };
