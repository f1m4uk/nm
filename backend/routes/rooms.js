import express from "express";

import { RoomsController } from "../controllers/RoomsControllers";
import { checkAuth } from "../middlewares/check-auth";
import { adminOnly } from "../middlewares/admin-only";

const roomsRouter = express.Router();

roomsRouter.post('', checkAuth, adminOnly, RoomsController.createRoom);

roomsRouter.get('/:id', checkAuth, adminOnly, RoomsController.readRoom);

roomsRouter.get('', checkAuth, adminOnly, RoomsController.readRooms);

roomsRouter.put('/:id', checkAuth, adminOnly, RoomsController.updateRoom);

roomsRouter.delete('/:id', checkAuth, adminOnly, RoomsController.deleteRoom);

export { roomsRouter }
