import express from "express";

import { RoomTypesController } from "../controllers/RoomTypesController";
import { adminOnly } from "../middlewares/admin-only";
import { checkAuth } from "../middlewares/check-auth";
import { extractFile } from "../middlewares/extract-file";

const roomTypesRouter = express.Router();

roomTypesRouter.post('', checkAuth, adminOnly, extractFile.array('images'), RoomTypesController.createRoomType);

roomTypesRouter.get('/:id', RoomTypesController.readRoomType);

roomTypesRouter.get('', RoomTypesController.readRoomTypes);

roomTypesRouter.put('/:id', checkAuth, adminOnly, extractFile.array('images'), RoomTypesController.updateRoomType);

roomTypesRouter.delete('/:id', checkAuth, adminOnly, RoomTypesController.deleteRoomType);

export { roomTypesRouter }
