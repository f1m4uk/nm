import express from "express";

import { UsersController } from "../controllers/UsersController";
import { checkAuth } from "../middlewares/check-auth";

const usersRouter = express.Router();

usersRouter.get('/current-user', checkAuth, UsersController.readCurrentUser);
usersRouter.get('/current-user/refresh-session', checkAuth, UsersController.refreshSession);
usersRouter.get('/current-user/guests', checkAuth, UsersController.readGuestsOfCurrentUser);

// usersRouter.post('', UsersController.createUser);

usersRouter.post('/login', UsersController.loginUser);


export { usersRouter }
