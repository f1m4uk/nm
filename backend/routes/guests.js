import express from "express";

import { GuestsController } from "../controllers/GuestsController";
import { checkAuth } from "../middlewares/check-auth";
import { adminOnly } from "../middlewares/admin-only";

const guestsRouter = express.Router();

guestsRouter.post('', checkAuth, adminOnly, GuestsController.createGuest);

guestsRouter.get('/:id', checkAuth, adminOnly, GuestsController.readGuest);

guestsRouter.get('', checkAuth, adminOnly, GuestsController.readGuests);

guestsRouter.put('/:id', checkAuth, adminOnly, GuestsController.updateGuest);

guestsRouter.delete('/:id', checkAuth, adminOnly, GuestsController.deleteGuest);

export { guestsRouter }
