import express from "express";

import { OffersController } from "../controllers/OffersController";
import { checkAuth } from "../middlewares/check-auth";
import { adminOnly } from "../middlewares/admin-only";

const offersRouter = express.Router();

offersRouter.post('', checkAuth, adminOnly, OffersController.createOffer);

offersRouter.get('/:id', checkAuth, OffersController.readOffer);

offersRouter.get('', checkAuth, OffersController.readOffers);

offersRouter.put('/:id', checkAuth, adminOnly, OffersController.updateOffer);

offersRouter.delete('/:id', checkAuth, adminOnly, OffersController.deleteOffer);

export { offersRouter }
