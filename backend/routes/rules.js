import express from "express";

import { RulesController } from "../controllers/RulesController";
import { checkAuth } from "../middlewares/check-auth";
import { adminOnly } from "../middlewares/admin-only";

const rulesRouter = express.Router();

rulesRouter.post('', checkAuth, adminOnly, RulesController.createRule);

rulesRouter.get('/:id', checkAuth, RulesController.readRule);

rulesRouter.get('', checkAuth, RulesController.readRules);

rulesRouter.put('/:id', checkAuth, adminOnly, RulesController.updateRule);

rulesRouter.delete('/:id', checkAuth, adminOnly, RulesController.deleteRule);

export { rulesRouter }
