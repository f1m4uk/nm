import express from "express";

import { ServicesController } from "../controllers/ServicesController";
import { adminOnly } from "../middlewares/admin-only";
import { checkAuth } from "../middlewares/check-auth";
import { extractFile } from "../middlewares/extract-file";

const servicesRouter = express.Router();

servicesRouter.post('', checkAuth, adminOnly, extractFile.array('images'), ServicesController.createService);

servicesRouter.get('/:id', ServicesController.readService);

servicesRouter.get('', ServicesController.readServices);

servicesRouter.put('/:id', checkAuth, adminOnly, extractFile.array('images'), ServicesController.updateService);

servicesRouter.delete('/:id', checkAuth, adminOnly, ServicesController.deleteService);

export { servicesRouter }
