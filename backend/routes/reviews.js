import express from "express";

import { ReviewsController } from "../controllers/ReviewsController";
import { checkAuth } from "../middlewares/check-auth";
import { adminOnly } from "../middlewares/admin-only";
import { extractFile } from "../middlewares/extract-file";

const reviewsRouter = express.Router();

reviewsRouter.post('', checkAuth, extractFile.single('image'), ReviewsController.createReview);

reviewsRouter.get('/:id', checkAuth, adminOnly, ReviewsController.readReview);

reviewsRouter.get('', ReviewsController.readReviews);

reviewsRouter.put('/:id', checkAuth, adminOnly, extractFile.single('image'), ReviewsController.updateReview);

reviewsRouter.delete('/:id', checkAuth, adminOnly, ReviewsController.deleteReview);

export { reviewsRouter }
