import jwt from 'jsonwebtoken';

const adminOnly = (request, response, next) => {
  const verifiedJsonWebToken = request.verifiedJsonWebToken;
  if (!verifiedJsonWebToken) {
    return response.status(401).json({
      message: 'Authentication failed'
    });
  }
  const decodedToken = jwt.decode(verifiedJsonWebToken);
  if (decodedToken.phoneNumber !== process.env.ADMIN_PHONE_NUMBER || decodedToken.role !== 'admin') {
    return response.status(401).json({
      message: 'Authentication failed'
    });
  }

  next();
}

export { adminOnly }
