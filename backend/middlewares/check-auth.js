import jwt from 'jsonwebtoken';

const checkAuth = (request, response, next) => {
  try {
    const token = request.headers.authorization.split(' ')[1];
    jwt.verify(token, process.env.JWT_SECRET);
    request.verifiedJsonWebToken = token;
    next();
  } catch(error) {
    response.status(401).json({
      message: 'Authentication failed'
    });
  }
}

export { checkAuth }
