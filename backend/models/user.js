import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const userSchema = mongoose.Schema({
  phoneNumber: { type: String, required: true, unique: true },
  avatarUrl: String,
  password: String // not required for purpose
});

userSchema.plugin(uniqueValidator);

const User = mongoose.model('User', userSchema);

export { User }
