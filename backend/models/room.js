import mongoose from 'mongoose';

const roomSchema = mongoose.Schema({
  name_en: { type: String },
  name_ru: { type: String, required: true },
  name_uk: { type: String },
  type: { type: mongoose.Schema.Types.ObjectId, ref: 'RoomType', required: true }
});

const Room = mongoose.model('Room', roomSchema);

export { Room }
