import mongoose from "mongoose";

const guestSchema = mongoose.Schema({
  phoneNumber: { type: String, required: true },
  peopleAmount: { type: Number, required: true },
  room: { type: mongoose.Schema.Types.ObjectId, ref: 'Room', required: true },
  notes: String,
  startDate: { type: Date, required: true },
  endDate: { type: Date, required: true },
  isReservation: { type: Boolean, default: true }
});

const Guest = mongoose.model('Guest', guestSchema);

export { Guest }
