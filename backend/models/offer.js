import mongoose from 'mongoose';

const offerSchema = mongoose.Schema({
  icon: String,
  name_en: { type: String, },
  name_ru: { type: String, required: true },
  name_uk: { type: String, },
  description_en: String,
  description_ru: String,
  description_uk: String
});

const Offer = mongoose.model('Offer', offerSchema);

export { Offer }
