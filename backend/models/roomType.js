import mongoose from "mongoose";

const roomTypeSchema = mongoose.Schema({
  name_en: { type: String },
  name_ru: { type: String, required: true },
  name_uk: { type: String },
  description_en: { type: String },
  description_ru: { type: String, required: true },
  description_uk: { type: String },
  services: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Service' }],
  imagePaths: [{ type: String }]
});

const RoomType = mongoose.model('RoomType', roomTypeSchema);

export { RoomType }
