import mongoose from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const reviewSchema = mongoose.Schema({
  user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true, required: true },
  authorName: { type: String, maxlength: 30, required: true },
  description: { type: String, maxlength: 200 },
  imagePath: String,
  mark: { type: Number, required: true, enum: [0, 1, 2, 3, 4, 5] },
  isApproved: { type: Boolean, default: false },
  adminNotes: String,
  date: { type: Date, immutable: true }
});

reviewSchema.plugin(uniqueValidator);

const Review = mongoose.model('Review', reviewSchema);

export { Review }
