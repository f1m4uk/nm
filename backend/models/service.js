import mongoose from 'mongoose';

const serviceSchema = mongoose.Schema({
  icon: String,
  name_en: { type: String },
  name_ru: { type: String, required: true },
  name_uk: { type: String },
  fullDescription_en: String,
  fullDescription_ru: String,
  fullDescription_uk: String,
  shortDescription_en: String,
  shortDescription_ru: String,
  shortDescription_uk: String,
  isRoomService: { type: Boolean, default: false },
  isHotelService: { type: Boolean, default: false },
  isAdditionalService: { type: Boolean, default: false },
  imagePaths: [{ type: String }]
});

const Service = mongoose.model("Service", serviceSchema);

export { Service }
