import mongoose from 'mongoose';

const ruleSchema = mongoose.Schema({
  description_en: { type: String },
  description_ru: { type: String, required: true },
  description_uk: { type: String }
});

const Rule = mongoose.model('Rule', ruleSchema);

export { Rule }
